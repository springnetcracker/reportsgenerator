package com.rpgen.core.service;

import org.apache.log4j.Logger;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by denis on 15.04.16.
 */
@Controller
@Configurable
@Service
public class ReportControllerService {
    Logger logger = Logger.getLogger(ReportControllerService.class);
    @Autowired
    AmqpTemplate template;

    public ReportControllerService(){
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @ResponseBody
    public boolean queue1(List<String> list) {
        logger.info("Emit to queue1");
        try {
            template.convertAndSend("queue1", list);
            return true;
        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }
}
