package com.rpgen.core.service;


import com.rpgen.core.hbnt.dao.UserDAO;


import com.rpgen.core.hbnt.entities.RolesEntity;
import com.rpgen.core.hbnt.entities.UsersEntity;
import org.apache.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by denis on 28.03.16.
 *
 */

public class LoginService implements UserDetailsService {

    private static final Logger logger = Logger.getLogger(LoginService.class);
    //private UserDao userDao = new UserDaoImpl();
    private UserDAO ud = new UserDAO();

    /**
     * Get authorisation for user
     * @param login
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String login) {

        UsersEntity user = ud.findUserByLogin(login);
        if (user==null) { //if user with same login does not exist
            logger.warn("User " + login + " not found");
            throw new UsernameNotFoundException("User not found");
        } else { //if user with same login exists, but his password is not correct
            List<GrantedAuthority> authorities = buildUserAuthority(user.getRoles());
            return buildUserForAuthentication(user, authorities);

       }
    }

    private User buildUserForAuthentication(UsersEntity user, List<GrantedAuthority> authorities) {
        return new User(user.getLogin(), user.getPassword(), true, true, true, true, authorities);
    }

    private List<GrantedAuthority> buildUserAuthority(Set<RolesEntity> userRoles) {

        Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

        // Build user's authorities
        for (RolesEntity userRole : userRoles) {
            setAuths.add(new SimpleGrantedAuthority(userRole.getRoleName()));
        }

        List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);
        return Result;
    }

}


