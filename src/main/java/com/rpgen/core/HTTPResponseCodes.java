package com.rpgen.core;

/**
 * Created by denis on 04.04.16.
 *
 */
public final  class HTTPResponseCodes {
    private HTTPResponseCodes(){

    }

    public static final int INTERNAL_ERROR = 500;
    public static final int CONFLICT = 409;
    public static final int OK = 200;
    public static final int NO_CONTENT = 204;
    public static final int BAD_REQUEST =400;
    public static final int NOT_FOUND=404;

}
