package com.rpgen.core.externdb;

import com.rpgen.core.model.*;
import com.rpgen.core.model.Connection;

import javax.ejb.Singleton;
import javax.ejb.Stateless;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Никита on 13.04.2016.
 */
@Stateless
public class MyDB {

    public MyDB() {
    }



    public java.sql.Connection getSqlConnection(Connection connection){
        switch (connection.getdBType().toLowerCase()) {
            case "oracle":
                return new Oracle().getSqlConnection(connection);
            case "mysql":
                return new MySql().getSqlConnection(connection);
            case "postgresql":
                return new PostgreSql().getSqlConnection(connection);
            default:
                return null;
        }
    }


    public boolean testConnection(java.sql.Connection connectionSQL) {
        if (connectionSQL==null) {
            return false;
        } else {
            return true;
        }
    }

    public List<String[]> requestData(java.sql.Connection connectionSQL, Template template) {

        List<String[]> listOfColumns=null;
        Statement stmt = null;
        ResultSet rs = null;

        try {

            stmt = connectionSQL.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            String query = template.getTemplateQuery();
            rs = stmt.executeQuery(query);
            int size = 0;
            rs.last();
            size = rs.getRow();
            rs.beforeFirst();

            if (size>0) {
                ResultSetMetaData rsmd = rs.getMetaData();
                int numberOfColumns = rsmd.getColumnCount();
                String[] row = new String[numberOfColumns]; //для получение массива из строки
                listOfColumns = new ArrayList<>(); //для полного select

                //запись заголовков
                for (int i = 0; i < numberOfColumns; i++) {
                    row[i] = rsmd.getColumnLabel(i + 1);
                }
                listOfColumns.add(row);

                while (rs.next()) {
                    row = new String[numberOfColumns];
                    for (int i = 0; i < numberOfColumns; i++) {
                        if (!(rs.getString(i + 1)==null)) {
                            row[i] = rs.getString(i + 1);
                        } else {
                            row[i] = "";
                        }
                    }
                    listOfColumns.add(row);
                }


            }
            rs.close();
            stmt.close();
            stmt.close();
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try { rs.close(); } catch (Exception ignore) {}
            try { stmt.close(); } catch (Exception ignore) {}
            try { connectionSQL.close(); } catch (Exception ignore) {}
        }

        return listOfColumns;
    }
}
