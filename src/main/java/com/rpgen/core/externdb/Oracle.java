package com.rpgen.core.externdb;

import com.rpgen.core.model.Connection;


import java.sql.*;

/**
 * Created by Никита on 13.04.2016.
 *
 */


public class Oracle extends MyDB{

    public Oracle() {
    }

    final public String[] selectGroup = new String[] {"group by","having","order by","procedure"};

    public java.sql.Connection getSqlConnection(Connection connection) {

        java.sql.Connection conn = null;

        try {

            String userName = connection.getdBUsername();
            String password = connection.getdBPassword();
            String stringAccess = stringAccess(connection);
            conn = DriverManager.getConnection(stringAccess, userName, password);

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return conn;
    }

    public String stringAccess(Connection connection) {
        String hostIp = connection.getHost();
        int port = connection.getPort();
        String sid = connection.getSid();
        String stringAccess = "jdbc:oracle:thin:@"+hostIp+":"+port+":"+sid;
        return stringAccess;
    }
}
