package com.rpgen.core.externdb;

import com.rpgen.core.model.Connection;


import javax.ejb.Singleton;
import javax.ejb.Stateless;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 * Created by Никита on 12.04.2016.
 */
public class MySql extends MyDB{

    public MySql() {
    }

    public java.sql.Connection getSqlConnection(Connection connection) {

        java.sql.Connection conn = null;

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String stringAccess = stringAccess(connection);
            conn = DriverManager.getConnection(stringAccess);

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return conn;
    }


    public String stringAccess(Connection connection) {
        String hostIp = connection.getHost();
        int port = connection.getPort();
        String nameDB = connection.getdBName();
        String userName = connection.getdBUsername();
        String password = connection.getdBPassword();
        String stringAccess = "jdbc:mysql://"+hostIp+":"+port+"/"+nameDB+"?user="+userName+"&password="+password;
        return stringAccess;
    }
}
