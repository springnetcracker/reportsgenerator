package com.rpgen.core.externdb;

import com.rpgen.core.model.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Никита on 03.05.2016.
 */
public class PostgreSql extends MyDB {

    public java.sql.Connection getSqlConnection(Connection connection) {

        java.sql.Connection conn = null;

        try {
            String userName = connection.getdBUsername();
            String password = connection.getdBPassword();
            String stringAccess = stringAccess(connection);
            conn = DriverManager.getConnection(stringAccess,userName,password);

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        return conn;
    }

    public String stringAccess(Connection connection) {
        String hostIp = connection.getHost();
        int port = connection.getPort();
        String nameDB = connection.getdBName();
        String stringAccess = "jdbc:postgresql://"+hostIp+":"+port+"/"+nameDB;
        return stringAccess;
    }
}
