package com.rpgen.core.rest;

import com.rpgen.core.ejb.TemplateBean;
import com.rpgen.core.hbnt.dao.TemplateDAO;
import com.rpgen.core.model.Template;

import static com.rpgen.core.HTTPResponseCodes.*;

import javax.inject.Inject;
import javax.jws.WebParam;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by denis on 26.03.16.
 *
 */
@Path("core/template")
public class TemplateRest {

    @Inject
    private TemplateBean templateBean;

    /**
     * Create new Template
     */
    @POST
    @Consumes("application/json")
    public Response createTemplate(Template template) {
        int result = templateBean.create(template);
        return Response.status(result).build();
    }

    /**
     * Get template by template_id
     *
     * @param template_id
     */
    @GET
    @Path("{template_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Template getTemplate(@Context SecurityContext sc, @PathParam("template_id") int template_id) {
        Template template = templateBean.getTemplate(template_id); //get template by id from DB
        return template;

    }

    /**
     * delete template by template_id
     *
     * @param template_id
     */
    @DELETE
    @Path("{template_id}")
    public Response deleteTemplate(@PathParam("template_id") int template_id) {
        //if report_id does not exist in DB, then response must be with code 500 (Resposnse.serverError().build)
        int result = templateBean.deleteTemplate(template_id);
        return Response.status(result).build();
    }


    @GET
    @Path("alltemplatesuser/{login}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllTemplatesByUser(@Context SecurityContext sc, @PathParam("login") String login) {
        if (sc.getUserPrincipal().getName().equals(login)) {
            List<Template> allTemplates;
            allTemplates = templateBean.getAllTemplatesByUser(sc.getUserPrincipal().getName());
            if (allTemplates.isEmpty()) {
                return Response.noContent().build();
            } else {
                return Response.status(OK).entity(allTemplates).build();
            }
        } else return Response.status(Response.Status.NOT_FOUND).build();
    }

    /**
     * Get all templates for current connection by connection_id
     *
     * @param connection_id
     */
    @GET
    @Path("alltemplatesconn")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllTemplateForCurrConn(@Context SecurityContext sc, @QueryParam("connection_id") int connection_id) {
        List<Template> result = templateBean.getAllTemplatesByConnection(sc.getUserPrincipal().getName(), connection_id);
        if ((result!=null) && (!result.isEmpty())) {
            return Response.status(OK).entity(result).build();
        } else return Response.status(NO_CONTENT).build();
    }


    /**
     * Check valid SQL select and send report params
     *
     * @param template
     * @return Json object with result validation and array params for template
     */
    @POST
    @Path("check")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response checkQueryAndGetParams(Template template) {
        String checkQuery = templateBean.checkQuery(template);
        if (checkQuery.contains("Valid")) {
            return Response.ok(checkQuery).build(); //всегда возвращает 200
        } else {
            return Response.status(Response.Status.BAD_REQUEST).entity(checkQuery).build();
        }
    }


}
