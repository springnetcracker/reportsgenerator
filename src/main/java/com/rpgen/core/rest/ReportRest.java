package com.rpgen.core.rest;

import com.rpgen.core.ejb.ReportBean;
import com.rpgen.core.ejb.UserBean;
import com.rpgen.core.ejb.pdf.PDFGenerator;
import com.rpgen.core.model.Report;
import com.rpgen.core.model.Template;
import com.rpgen.core.service.ReportControllerService;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Map;

import static com.rpgen.core.HTTPResponseCodes.*;

import static com.rpgen.core.HTTPResponseCodes.*;

/**
 * Created by denis on 26.03.16.
 *
 */
@Path("core/report")
public class ReportRest {

    @Inject
    private ReportBean reportBean;

    @Inject
    private UserBean userBean;

    /**
     * Create new Report
     */
    @POST
    @Consumes("application/json")
    public Response createReport( Report report){
        int result = reportBean.create(report);
        return Response.status(result).build(); //if report successfully create
    }

    /**
     * Get report by report_id
     * @param report_id
     */
    @GET
    @Path("{report_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReport(@Context SecurityContext sc, @PathParam("report_id") int report_id) {
        Report report = reportBean.getReport(sc.getUserPrincipal().getName(),report_id); //get report by id from DB
        if (report!=null){
            return Response.status(Response.Status.OK).entity(report).build();
        } else
        {
            return Response.noContent().build();
        }
    }

    /**
     * update report by report_id
     * @param report_id
     */
    @PUT
    @Path("upd/{report_id}")
    public Response updateReport(@PathParam("report_id") int report_id, byte[] json){
        int result = reportBean.update(report_id, json);
        return Response.ok().build(); //if report successfully update

    }

    /**
     * delete report by report_id
     * @param report_id
     */
    @DELETE
    @Path("{report_id}")
    public Response deleteReport(@PathParam("report_id") int report_id){
        //if report_id does not exist in DB, then response must be with code 500 (Resposnse.serverError().build)
        int result = reportBean.deleteReport(report_id);
        return Response.status(result).build(); //response code 204
    }

    /**
     * return all user's reports (for current session)
     * @return
     */
    @GET
    @Path("allreport/{login}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllUserReports(@Context SecurityContext sc,@PathParam("login")String login){ //must retur List if reports in JSON
        if (!login.equals(sc.getUserPrincipal().getName())){
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            List<Report> result = reportBean.getAllReports(sc.getUserPrincipal().getName());
            if (result.isEmpty()) {
                return Response.noContent().build();
            } else {
                return Response.status(OK).entity(result).build();
            }
        }
    }


    /**
     * void connecting to database by connection_id in template
     * and complete query from template_query
     *
     * @param template_id templates id
     * @return Json object of array titles and arrays rows
     */
    /*
    @GET
    @Path("create/{template_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response dataFromQuery(@PathParam("template_id") int template_id) {
        StringBuilder response = reportBean.requestData(template_id);
        if (response!=null) {
            return Response.ok(response.toString()).build();
        } else {
            return Response.status(400).build();
        }
    }*/

    /**
     * return pdf file for download it with results of resport
     */
    @GET
    @Path("pdf/{report_id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getPDF(@Context SecurityContext sc, @PathParam("report_id") int report_id){
        File file=null;
        try {
            file= new PDFGenerator().getPDF(sc.getUserPrincipal().getName(), report_id);
        } catch (Exception e){
            e.printStackTrace();
        }
        if (file!=null) {
            return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM)
                    .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"") //optional
                    .build();
        } else return Response.noContent().build();
    }





}
