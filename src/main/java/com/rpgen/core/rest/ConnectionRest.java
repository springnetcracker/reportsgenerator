package com.rpgen.core.rest;

import com.rpgen.core.ejb.ConnectionBean;
import com.rpgen.core.ejb.UserBean;
import com.rpgen.core.model.Connection;
import com.rpgen.core.model.User;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.List;
import static com.rpgen.core.HTTPResponseCodes.*;

/**
 * Created by Никита on 31.03.2016.
 *
 */

@Path("core/connection")
public class ConnectionRest {

    @Inject
    private ConnectionBean connectionBean;

    @Inject
    UserBean userBean;

    @Context
    private UriInfo context;

    public ConnectionRest() {
    }


    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getConnection(@Context SecurityContext sc, @PathParam("id") int id) {
        User user =  userBean.getUser(sc.getUserPrincipal().getName());
        Connection connection = connectionBean.getConnection(id);
        if ((connection!=null) && ((user.getUserId() == connection.getUserId()))){
            return Response.status(OK).entity(connection).build();
        } else {
            return Response.noContent().build();
        }
    }

    /**
     *  call void creating Connection in Database
     *  and redirect to /connection/{id}
     */
    @POST
    @Consumes("application/json")
    public Response createConnection(Connection connection) {

        int idConnection = connectionBean.create(connection);
        return Response.status(idConnection).build();
    }

    /**
     *  call void changing Connection in Database
     *  and redirect to /connection/{id}
     */
    @PUT
    @Consumes("application/json")
    @Path("/{id}")
    public Response changeConnection(@PathParam("id") int id, Connection connection) {
        int result = connectionBean.update(connection);
        return Response.status(result).build();
    }

    /**
     *  call void delete Connection in Database
     *  and redirect to /connection
     */
    @DELETE
    @Path("/{id}")
    public Response deleteConnection(@PathParam("id") int id) {
        int result = connectionBean.deleteConnection(id);
        return Response.status(result).build();
    }

    /**
     *  call void checking ping Connection
     *  and send 200 if ping is there or 400 if ping no
     */
    @POST
    @Path("/test")
    @Produces("application/json")
    public Response testConnection(Connection connection) {
        boolean isPing = connectionBean.testConnection(connection);
        if (isPing) {
            return Response.ok().build();
        } {
            return Response.status(400).build();
        }
    }

    /**
     * Return all connections for User ( List of connections)
     * @return
     */
    @GET
    @Path("allconn/{login}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllConnectionsForUser(@Context SecurityContext sc, @PathParam("login") String login){//return List of connections for current session' user (in JSON)
        if (!login.equals(sc.getUserPrincipal().getName())){
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            List<Connection> connectionList = connectionBean.getAllConnections(sc.getUserPrincipal().getName());
            if (connectionList.isEmpty())
                return Response.status(NO_CONTENT).build();
            else
                return Response.status(OK).entity(connectionList).build();
        }
    }


}
