package com.rpgen.core.rest;

import com.rpgen.core.ejb.UserBean;
import com.rpgen.core.model.User;

import javax.inject.Inject;
import javax.ws.rs.*;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import static com.rpgen.core.HTTPResponseCodes.*;

@Path("core/")
public class UserREST{

    @Inject
    UserBean userBean;

    /**
     * creating user from @FormParams
     * if user has been created successfully he will be redirected to /user/{id}
     */
    @POST @Consumes("application/json")
    @Path("register")
    public Response createUser(User user){
        int response = INTERNAL_ERROR;

        try {
            response = userBean.create(user);
        } catch (Exception e){
            e.printStackTrace();
        }
        return Response.status(response).build(); //All ok

    }


    /**
     * if user is logged in he is redirected to /user/{id}
     * otherwise he is redirected to /user/login
     */
    @GET
    @Path("{login}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserByLogin(@Context SecurityContext sc,@javax.ws.rs.PathParam("login") String login) {
        if (!login.equals(sc.getUserPrincipal().getName())){
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            User result = userBean.getUser(sc.getUserPrincipal().getName());
            if (result != null) {
                return Response.status(OK).entity(result).build();
            } else {
                return Response.noContent().build();
            }
        }
    }

}

