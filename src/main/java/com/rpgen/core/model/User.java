package com.rpgen.core.model;

import java.io.Serializable;

/**
 * Created by denis on 31.03.16.
 */
public class User implements MyModel{
    private int userId;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String email;


    public  User(){

    }
    public User (String login, String password, String firstName, String lastName, String email){
        this.login=login;
        this.password=password;
        this.firstName=firstName;
        this.lastName=lastName;
        this.email=email;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
