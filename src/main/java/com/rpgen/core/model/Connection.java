package com.rpgen.core.model;

import java.util.Date;


/**
 * Created by Никита on 31.03.2016.
 */
public class Connection implements MyModel{

    private int connectionId;
    private  int userId;
    private String connectionName;
    private String dBType;
    private Date connectionDate;
    private String host;
    private  String dBUsername;
    private  String dBPassword;
    private int port;
    private String sid;
    private int isDeleted;
    private String dBName;

    public Connection() {

    }

    public Connection(int connectionId, int userId, String connectionName, String dBType, String host, String dBUsername, String dBPassword, int port, String sid, String dBName) {
        this.connectionId = connectionId;
        this.userId = userId;
        this.connectionName = connectionName;
        this.dBType = dBType;
        this.host = host;
        this.dBUsername = dBUsername;
        this.dBPassword = dBPassword;
        this.port = port;
        this.sid = sid;
        isDeleted=0;
        this.dBName=dBName;
    }

    public int getConnectionId() {
        return connectionId;
    }

    public int getUserId() {
        return userId;
    }

    public String getConnectionName() {
        return connectionName;
    }

    public String getdBType() {
        return dBType;
    }

    public Date getConnectionDate() {
        return connectionDate;
    }

    public String getHost() {
        return host;
    }

    public String getdBUsername() {
        return dBUsername;
    }

    public String getdBPassword() {
        return dBPassword;
    }

    public int getPort() {
        return port;
    }

    public String getSid() {
        return sid;
    }

    public void setConnectionId(int connectionId) {
        this.connectionId = connectionId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setConnectionName(String connectionName) {
        this.connectionName = connectionName;
    }

    public void setdBType(String dBType) {
        this.dBType = dBType;
    }

    public void setConnectionDate(Date connectionDate) {
        this.connectionDate = connectionDate;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setdBUsername(String dBUsername) {
        this.dBUsername = dBUsername;
    }

    public void setdBPassword(String dBPassword) {
        this.dBPassword = dBPassword;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getdBName() {
        return dBName;
    }

    public void setdBName(String dBName) {
        this.dBName = dBName;
    }
}
