package com.rpgen.core.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by denis on 06.04.16.
 */
public class Report implements MyModel{
    private String reportName;
    private String reportDescription;
    private String templateName;
    private Date reportDate;
    private int templateID;
    private int userID;
    private int isDeleted;
    private int status;
    private int reportId;
    private Byte[] resultReport;
    private Map <Integer, String> reportValues;

    public Report(){

    }
    public Report(String reportName, String reportDescription, int templateID){
        this.reportName=reportName;
        this.reportDescription=reportDescription;
        this.templateID=templateID;
        isDeleted=0;
        status=0;
    }

    public Byte[] getResultReport () {return resultReport;}

    public void setResultReport (Byte[] resultReport) {this.resultReport = resultReport;}

    public Map <Integer, String> getReportValues () {return reportValues;}

    public void setReportValues (Map <Integer, String> values) {reportValues = values;}

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getReportDescription() {
        return reportDescription;
    }

    public void setReportDescription(String reportDescription) {
        this.reportDescription = reportDescription;
    }


    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public int getTemplateID() {
        return templateID;
    }

    public void setTemplateID(int templateID) {
        this.templateID = templateID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getReportId() {
        return reportId;
    }

    public void setReportId(int reportId) {
        this.reportId = reportId;
    }
}
