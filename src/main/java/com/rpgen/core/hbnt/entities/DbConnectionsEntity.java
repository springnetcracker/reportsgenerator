package com.rpgen.core.hbnt.entities;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

/**
 * Created by denis on 03.04.16.
 */

@Entity
@Table(name = "DB_CONNECTIONS", schema = "COREDB")
public class DbConnectionsEntity implements MyEntity, Serializable {
    private Integer connectionId;
    private UsersEntity userIdConnection;
    private String connectionName;
    private String dbType;
    private Date connectionDate;
    private String hostIp;
    private String dbUsername;
    private String dbPassword;
    private Integer port;
    private String sid;
    private Set<TemplatesEntity> templatesEntitySet;
    private Integer isDeleted;
    private String dBName;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONNECTION_ID_SEQ")
    @SequenceGenerator(name="CONNECTION_ID_SEQ", sequenceName = "CONNECTION_ID_SEQ", allocationSize = 1)
    @Column(name = "CONNECTION_ID", nullable = false, precision = 0)
    public Integer getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(Integer connectionId) {
        this.connectionId = connectionId;
    }

    @Basic
    @Column(name = "CONNECTION_NAME", nullable = false, length = 100)
    public String getConnectionName() {
        return connectionName;
    }

    public void setConnectionName(String connectionName) {
        this.connectionName = connectionName;
    }

    @Basic
    @Column(name = "DB_TYPE", nullable = false, precision = 0)
    public String getDbType() {
        return dbType;
    }

    public void setDbType(String dbType) {
        this.dbType = dbType;
    }

    @Basic
    @Column(name = "CONNECTION_DATE", nullable = false)
    public Date getConnectionDate() {
        return connectionDate;
    }

    public void setConnectionDate(Date connectionDate) {
        this.connectionDate = connectionDate;
    }

    @Basic
    @Column(name = "HOST_IP", nullable = false, length = 30)
    public String getHostIp() {
        return hostIp;
    }

    public void setHostIp(String hostIp) {
        this.hostIp = hostIp;
    }

    @Basic
    @Column(name = "DB_USERNAME", nullable = false, length = 100)
    public String getDbUsername() {
        return dbUsername;
    }

    public void setDbUsername(String dbUsername) {
        this.dbUsername = dbUsername;
    }

    @Basic
    @Column(name = "DB_PASSWORD", nullable = false, length = 100)
    public String getDbPassword() {
        return dbPassword;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    @Basic
    @Column(name = "PORT", nullable = false, precision = 0)
    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    @Basic
    @Column(name = "SID", nullable = false, length = 20)
    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DbConnectionsEntity that = (DbConnectionsEntity) o;

        if (connectionId != null ? !connectionId.equals(that.connectionId) : that.connectionId != null) return false;
        if (userIdConnection != null ? !userIdConnection.equals(that.userIdConnection) : that.userIdConnection != null) return false;
        if (connectionName != null ? !connectionName.equals(that.connectionName) : that.connectionName != null)
            return false;
        if (dbType != null ? !dbType.equals(that.dbType) : that.dbType != null) return false;
        if (connectionDate != null ? !connectionDate.equals(that.connectionDate) : that.connectionDate != null)
            return false;
        if (hostIp != null ? !hostIp.equals(that.hostIp) : that.hostIp != null) return false;
        if (dbUsername != null ? !dbUsername.equals(that.dbUsername) : that.dbUsername != null) return false;
        if (dbPassword != null ? !dbPassword.equals(that.dbPassword) : that.dbPassword != null) return false;
        if (port != null ? !port.equals(that.port) : that.port != null) return false;
        if (sid != null ? !sid.equals(that.sid) : that.sid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = connectionId != null ? connectionId.hashCode() : 0;
        result = 31 * result + (userIdConnection != null ? userIdConnection.hashCode() : 0);
        result = 31 * result + (connectionName != null ? connectionName.hashCode() : 0);
        result = 31 * result + (dbType != null ? dbType.hashCode() : 0);
        result = 31 * result + (connectionDate != null ? connectionDate.hashCode() : 0);
        result = 31 * result + (hostIp != null ? hostIp.hashCode() : 0);
        result = 31 * result + (dbUsername != null ? dbUsername.hashCode() : 0);
        result = 31 * result + (dbPassword != null ? dbPassword.hashCode() : 0);
        result = 31 * result + (port != null ? port.hashCode() : 0);
        result = 31 * result + (sid != null ? sid.hashCode() : 0);
        return result;
    }

    //ref to table Users
    @ManyToOne
    @JoinColumn(name = "USER_ID")
    public UsersEntity getUserIdConnection() {
        return userIdConnection;
    }

    public void setUserIdConnection(UsersEntity userIdConnection) {
        this.userIdConnection = userIdConnection;
    }

    @OneToMany(mappedBy = "dbConnectionsEntity")
    public Set<TemplatesEntity> getTemplatesEntitySet() {
        return templatesEntitySet;
    }

    public void setTemplatesEntitySet(Set<TemplatesEntity> templatesEntitySet) {
        this.templatesEntitySet = templatesEntitySet;
    }

    @Basic
    @Column(name = "CONN_DELETED", nullable = false, length = 1)
    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Basic
    @Column(name = "DB_NAME", length = 100)
    public String getdBName() {
        return dBName;
    }

    public void setdBName(String dBName) {
        this.dBName = dBName;
    }
}
