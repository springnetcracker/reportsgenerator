package com.rpgen.core.hbnt.entities;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by denis on 08.04.16.
 */
@Entity
@Table(name = "TEMPLATES", schema = "COREDB")
public class TemplatesEntity implements MyEntity {
    private Integer templateId;
    private String templateName;
    private Date templateDate;
    private String query;
    private Set<ReportsEntity> reportsEntitySet;
    private Set<ReportParamsEntity> reportParamsEntitySet= new HashSet<>();
    private DbConnectionsEntity dbConnectionsEntity;
    private Integer isDeleted;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEMPLATES")
    @SequenceGenerator(name="TEMPLATES", sequenceName = "TEMPLATE_ID_SEQ", allocationSize = 1)
    @Column(name = "TEMPLATE_ID", nullable = false, precision = 0)
    public Integer getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Integer templateId) {
        this.templateId = templateId;
    }

    @Basic
    @Column(name = "TEMPLATE_NAME", nullable = false, length = 100)
    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    @Basic
    @Column(name = "TEMPLATE_DATE", nullable = false)
    public Date getTemplateDate() {
        return templateDate;
    }

    public void setTemplateDate(Date templateDate) {
        this.templateDate = templateDate;
    }

    @Basic
    @Column(name = "QUERY", nullable = false, length = 2000)
    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TemplatesEntity that = (TemplatesEntity) o;

        if (templateId != null ? !templateId.equals(that.templateId) : that.templateId != null) return false;
        if (templateName != null ? !templateName.equals(that.templateName) : that.templateName != null) return false;
        if (templateDate != null ? !templateDate.equals(that.templateDate) : that.templateDate != null) return false;
        if (query != null ? !query.equals(that.query) : that.query != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = templateId != null ? templateId.hashCode() : 0;
        result = 31 * result + (templateName != null ? templateName.hashCode() : 0);
        result = 31 * result + (templateDate != null ? templateDate.hashCode() : 0);
        result = 31 * result + (query != null ? query.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "templatesEntity")
    public Set<ReportsEntity> getReportsEntitySet() {
        return reportsEntitySet;
    }

    public void setReportsEntitySet(Set<ReportsEntity> reportsEntitySet) {
        this.reportsEntitySet = reportsEntitySet;
    }

    @ManyToOne
    @JoinColumn(name="CONNECTION_ID")
    public DbConnectionsEntity getDbConnectionsEntity() {
        return dbConnectionsEntity;
    }

    public void setDbConnectionsEntity(DbConnectionsEntity dbConnectionsEntity) {
        this.dbConnectionsEntity = dbConnectionsEntity;
    }

    @Basic
    @Column(name = "TEMPLATE_DELETED", nullable = false, length = 1)
    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    @OneToMany(cascade = CascadeType.ALL ,mappedBy = "templateId", fetch=FetchType.EAGER)
    public Set<ReportParamsEntity> getReportParamsEntitySet() {
        return reportParamsEntitySet;
    }

    public void setReportParamsEntitySet(Set<ReportParamsEntity> reportParamsEntitySet) {
        this.reportParamsEntitySet = reportParamsEntitySet;
    }

    public void addParam(ReportParamsEntity rp){

        //this.reportParamsEntitySet.add(reportParamsEntitySet);
        reportParamsEntitySet.add(rp);
    }
}
