package com.rpgen.core.hbnt.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by Никита on 08.04.2016.
 */
@Entity
@Table(name = "REPORT_PARAMS", schema = "COREDB")
public class ReportParamsEntity implements MyEntity {
    private Integer paramId;
    private String paramName;
    private String varParam;
    private String typeVarParam;
    private TemplatesEntity templateId;


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REPORT_PARAMS")
    @SequenceGenerator(name="REPORT_PARAMS", sequenceName = "REPORT_PARAM_SEQ", allocationSize = 1)
    @Column(name = "PARAM_ID", nullable = false, precision = 0)
    public Integer getParamId() {
        return paramId;
    }

    public void setParamId(Integer paramId) {
        this.paramId = paramId;
    }


    @Basic
    @Column(name = "PARAM_NAME", nullable = false, length = 100)
    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }



    @Basic
    @Column(name = "VAR_PARAM", nullable = false, length = 100)
    public String getVarParam() {
        return varParam;
    }

    public void setVarParam(String varParam) {
        this.varParam = varParam;
    }



    @Basic
    @Column(name = "TYPE_VAR_PARAM", nullable = false, length = 100)
    public String getTypeVarParam() {
        return typeVarParam;
    }

    public void setTypeVarParam(String typeVarParam) {
        this.typeVarParam = typeVarParam;
    }


    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "TEMPLATE_ID")
    public TemplatesEntity getTemplateId() {
        return templateId;
    }

    public void setTemplateId(TemplatesEntity templateId) {
        this.templateId = templateId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReportParamsEntity that = (ReportParamsEntity) o;

        if (!paramId.equals(that.paramId)) return false;
        if (!paramName.equals(that.paramName)) return false;
        if (!varParam.equals(that.varParam)) return false;
        if (!typeVarParam.equals(that.typeVarParam)) return false;
        return templateId.equals(that.templateId);

    }

    @Override
    public int hashCode() {
        int result = (paramId != null) ? paramId.hashCode() : 7;
        result = 31 * result + paramName.hashCode();
        result = 31 * result + varParam.hashCode();
        result = 31 * result + typeVarParam.hashCode();
        result = 31 * result + templateId.hashCode();
        return result;
    }
}
