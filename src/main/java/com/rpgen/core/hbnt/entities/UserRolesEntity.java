package com.rpgen.core.hbnt.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by denis on 31.03.16.
 */
@Entity
@Table(name = "USER_ROLES", schema = "COREDB")
public class UserRolesEntity implements MyEntity {
    private Integer userRoleId;
    private UsersEntity user;
    private Integer roleId;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_ROLE")
    @SequenceGenerator(name="USER_ROLE", sequenceName = "USER_ROLE_SEQ", allocationSize = 1)
    @Column(name = "USER_ROLE_ID", nullable = false, precision = 0)
    public Integer getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(Integer userRoleId) {
        this.userRoleId = userRoleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserRolesEntity that = (UserRolesEntity) o;

        if (userRoleId != null ? !userRoleId.equals(that.userRoleId) : that.userRoleId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {

        return userRoleId != null ? userRoleId.hashCode() : 0;
    }

    //ref to table Users
    @ManyToOne
    @JoinColumn(name = "USER_ID")
    public UsersEntity getUser() {
        return user;
    }

    public void setUser(UsersEntity user) {
        this.user = user;
    }

    @Column(name = "ROLE_ID", nullable = false, precision = 0)
    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
}
