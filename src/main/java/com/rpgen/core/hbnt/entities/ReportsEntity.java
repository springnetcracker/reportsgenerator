package com.rpgen.core.hbnt.entities;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Type;
import org.hibernate.metamodel.source.annotations.attribute.type.LobTypeResolver;

import javax.persistence.*;
import java.sql.Blob;
import java.util.Date;

/**
 * Created by denis on 06.04.16.
 */
@Entity
@Table(name = "REPORTS", schema = "COREDB", catalog = "")
public class ReportsEntity implements MyEntity{
    private UsersEntity userIdReports;
    private Integer reportId;
    private TemplatesEntity templatesEntity;
    private Integer isDeleted;
    private Integer status;
    private Byte[] reportResult;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONNECTION")
    @SequenceGenerator(name="CONNECTION", sequenceName = "CONNECTION_ID_SEQ", allocationSize = 1)
    @Column(name = "REPORT_ID", nullable = false, precision = 0)
    public Integer getReportId() {
        return reportId;
    }

    public void setReportId(Integer reportId) {
        this.reportId = reportId;
    }

    private String reportName;

    @Basic
    @Column(name = "REPORT_NAME", nullable = false, length = 100)
    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    private String description;

    @Basic
    @Column(name = "DESCRIPTION", nullable = true, length = 1000)
    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }

    @Type(type = "oracle_date")
    private Date dateReport;

    @Basic
    @Column(name = "DATE_REPORT", nullable = false)
    public Date getDateReport() {
        return dateReport;
    }

    public void setDateReport(Date dateReport) {
        this.dateReport = dateReport;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReportsEntity that = (ReportsEntity) o;

        if (reportId != null ? !reportId.equals(that.reportId) : that.reportId != null) return false;
        if (reportName != null ? !reportName.equals(that.reportName) : that.reportName != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (dateReport != null ? !dateReport.equals(that.dateReport) : that.dateReport != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = reportId != null ? reportId.hashCode() : 0;
        result = 31 * result + (reportName != null ? reportName.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (dateReport != null ? dateReport.hashCode() : 0);
        return result;
    }


    @ManyToOne
    @JoinColumn(name = "USER_ID")

    public UsersEntity getUserIdReports() {
        return userIdReports;
    }

    public void setUserIdReports(UsersEntity userIdReports) {
        this.userIdReports = userIdReports;
    }


    @ManyToOne
    @JoinColumn(name="TEMPLATE_ID")
    public TemplatesEntity getTemplatesEntity() {
        return templatesEntity;
    }

    public void setTemplatesEntity(TemplatesEntity templatesEntity) {
        this.templatesEntity = templatesEntity;
    }

    @Basic
    @Column(name = "REPORT_DELETED", nullable = false, length = 1)
    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Basic
    @Column(name = "STATUS", nullable = false, length = 1)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    @Column(name = "RESULT", columnDefinition = "blob")
    @Lob
    public Byte[] getReportResult() {
        return reportResult;
    }

    public void setReportResult(Byte[] reportResult) {
        this.reportResult = reportResult;
    }
}
