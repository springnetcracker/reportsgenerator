package com.rpgen.core.hbnt.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by denis on 29.03.16.
 */
@Entity
@Table(name = "USERS", schema = "COREDB", uniqueConstraints = @UniqueConstraint(columnNames = {"login"}))
public class UsersEntity implements MyEntity{
    private Integer userId;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private Set<UserRolesEntity> userRoles = new HashSet<>();
    private Set<RolesEntity> roles = new HashSet<>();
    private Set<DbConnectionsEntity> connections = new HashSet<>();
    private Set<ReportsEntity> userReports;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_ID_SEQ")
    @SequenceGenerator(name="USER_ID_SEQ", sequenceName = "USER_ID_SEQ", allocationSize = 1)
    @Column(name = "USER_ID", nullable = false, precision = 0)
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "LOGIN", nullable = false, length = 500)
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Basic
    @Column(name = "USER_PASSWORD", nullable = false, length = 1500)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "FIRST_NAME", nullable = true, length = 500)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "LAST_NAME", nullable = true, length = 500)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "EMAIL", nullable = false, length = 500)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UsersEntity that = (UsersEntity) o;

        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (login != null ? !login.equals(that.login) : that.login != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }
/*
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    public Set<UserRolesEntity> getUser() {
            return user;
    }

    public void setUser(Set<UserRolesEntity> user) {
        this.user = user;
    }
*/

    //ref to table ROLES
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "USER_ROLES",
            //foreign key for CarsEntity in employee_car table
            joinColumns = @JoinColumn(name = "USER_ID"),
            //foreign key for other side - EmployeeEntity in employee_car table
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID"))
    public Set<RolesEntity> getRoles() {
        return roles;
    }

    public void setRoles(Set<RolesEntity> roles) {
        this.roles = roles;
    }


    //ref to table User Roles
    @OneToMany(cascade = { CascadeType.ALL },mappedBy = "user")
    public Set<UserRolesEntity> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Set<UserRolesEntity> userRoles) {
        this.userRoles = userRoles;
    }

    public void addRole(UserRolesEntity userRoles){
        this.userRoles.add(userRoles);
    }

    //ref to table Connections
    @OneToMany(cascade = { CascadeType.ALL },mappedBy = "userIdConnection")
    public Set<DbConnectionsEntity> getConnections() {
        return connections;
    }

    public void setConnections(Set<DbConnectionsEntity> connections) {
        this.connections = connections;
    }

    @OneToMany(cascade = { CascadeType.ALL },mappedBy = "userIdReports")
    public Set<ReportsEntity> getUserReports() {
        return userReports;
    }

    public void setUserReports(Set<ReportsEntity> userReports) {
        this.userReports = userReports;
    }

}
