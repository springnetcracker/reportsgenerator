package com.rpgen.core.hbnt.dao;

/**
 * Created by denis on 24.04.16.
 *
 */
public class DAOFactory {
    private static UserDAO userDAO = null;
    private static ConnectionDAO connectionDAO = null;
    private static TemplateDAO templateDAO = null;
    private static ReportDAO reportDAO=null;
    private static DAOFactory instance = null;

    public static synchronized DAOFactory getInstance(){
        if (instance == null){
            instance = new DAOFactory();
        }
        return instance;
    }

    public UserDAO getUserDAO(){
        if (userDAO == null){
            userDAO = new UserDAO();
        }
        return userDAO;
    }

    public  ConnectionDAO getConnectionDAO() {
        if (connectionDAO==null){
            connectionDAO=new ConnectionDAO();
        }
        return connectionDAO;
    }

    public  TemplateDAO getTemplateDAO() {
        if (templateDAO==null){
            templateDAO=new TemplateDAO();
        }
        return templateDAO;
    }

    public  ReportDAO getReportDAO() {
        if (reportDAO==null){
            reportDAO=new ReportDAO();
        }
        return reportDAO;
    }
}
