package com.rpgen.core.hbnt.dao;

import com.rpgen.core.hbnt.entities.UsersEntity;
import org.hibernate.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by denis on 08.04.16.
 */
public class UserDAO extends EntityDAO {

    @Override
    protected String getTableName() {
        return "UsersEntity";
    }

    public UsersEntity findUserByLogin(String login) {
        List<UsersEntity> users = new ArrayList<UsersEntity>();
        Locale.setDefault(Locale.UK);
        try {
            String hql = "from UsersEntity where login=:login";
            Query query = HibernateUtil.getSessionFactory().openSession().createQuery(hql);
            query.setParameter("login", login);
            users = query.list();
        } catch (Exception e){
            e.printStackTrace();
        }

        if (users.size() > 0) {
            return users.get(0);
        } else {
            return null; //goes here, if user does not exist
        }
    }
}
