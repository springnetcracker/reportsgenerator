package com.rpgen.core.hbnt.dao;

import com.rpgen.core.hbnt.entities.DbConnectionsEntity;
import com.rpgen.core.hbnt.entities.MyEntity;
import com.rpgen.core.hbnt.entities.ReportParamsEntity;
import com.rpgen.core.hbnt.entities.TemplatesEntity;
import com.rpgen.core.model.Template;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import static com.rpgen.core.HTTPResponseCodes.INTERNAL_ERROR;
import static com.rpgen.core.HTTPResponseCodes.NO_CONTENT;
import static com.rpgen.core.HTTPResponseCodes.OK;

public class TemplateDAO extends EntityDAO {
    @Override
    protected String getTableName() {
        return "TemplatesEntity";
    }

    @SuppressWarnings({"unchecked", "JpaQlInspection"})
    public List<TemplatesEntity> getAllTemplatesByConnection(int connection_id) {
        List<TemplatesEntity> templatesList = new ArrayList<TemplatesEntity>();
        Session session = HibernateUtil.getSessionFactory().openSession();
        Locale.setDefault(Locale.UK);
        String hql = "from TemplatesEntity\n" +
                "where connection_id=:connection_id and isDeleted=0";
        try {
            Query query = session.createQuery(hql);
            query.setParameter("connection_id", connection_id);
            templatesList = query.list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            session.close();
        }
        return templatesList;
    }


    public List<TemplatesEntity> getAllTemplatesByUser(String login){
        List<TemplatesEntity> templatesList = new ArrayList<TemplatesEntity>();
        Locale.setDefault(Locale.UK);
        String hql = "select t from UsersEntity as u inner join u.connections as c inner join c.templatesEntitySet as t\n" +
                "where u.login=:login and t.isDeleted=0";
        templatesList=getAllEntityForUser(hql, login);
        return templatesList;
    }



}
