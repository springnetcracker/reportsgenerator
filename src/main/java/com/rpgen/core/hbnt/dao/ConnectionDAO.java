package com.rpgen.core.hbnt.dao;

import com.rpgen.core.hbnt.entities.DbConnectionsEntity;
import com.rpgen.core.hbnt.entities.MyEntity;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import static com.rpgen.core.HTTPResponseCodes.*;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by denis on 08.04.16.
 *
 */
public class ConnectionDAO extends EntityDAO {

    @Override
    protected String getTableName() {
        return "DbConnectionsEntity";
    }

    @SuppressWarnings({"unchecked", "JpaQlInspection"})
    public List<DbConnectionsEntity> getAllConnections(String login){
        List<DbConnectionsEntity> connections = new ArrayList<DbConnectionsEntity>();
        Locale.setDefault(Locale.UK);
        String hql = "select c from UsersEntity as u inner join u.connections as c\n" +
                "where u.userId=c.userIdConnection and u.login=:login and c.isDeleted=0";
        connections=getAllEntityForUser(hql, login);
        return connections;
    }

    @Override
    public boolean deleteEntity(int id) {
        int result=0;
        DbConnectionsEntity entity = (DbConnectionsEntity)getEntity(id);
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction=null;
        String hql="update DbConnectionsEntity set isDeleted = 1 where id=:id";
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("id", id);
            result+= query.executeUpdate();
            hql= "update TemplatesEntity set isDeleted = 1 where dbConnectionsEntity=:dbConnectionsEntity";
            query = session.createQuery(hql);
            query.setParameter("dbConnectionsEntity", entity);
            result+= query.executeUpdate();
            transaction.commit();
        } catch (Exception e){
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;
        } finally {
            session.close();
        }
        if (result>0) {
            return true;
        } else {
            return false;
        }
    }
}
