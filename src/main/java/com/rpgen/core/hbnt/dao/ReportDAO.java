package com.rpgen.core.hbnt.dao;

import com.rpgen.core.hbnt.entities.MyEntity;
import com.rpgen.core.hbnt.entities.ReportsEntity;
import com.rpgen.core.hbnt.entities.TemplatesEntity;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.GenericJDBCException;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.rpgen.core.HTTPResponseCodes.INTERNAL_ERROR;
import static com.rpgen.core.HTTPResponseCodes.NO_CONTENT;
import static com.rpgen.core.HTTPResponseCodes.OK;

/**
 * Created by denis on 08.04.16.
 */
public class ReportDAO extends EntityDAO{
    Integer reportId=0;
    @Override
    protected String getTableName() {
        return "ReportsEntity";
    }

    @SuppressWarnings({"unchecked", "JpaQlInspection"})
    public List<ReportsEntity> getAllReports(String login){
        List<ReportsEntity> reportsEntities = new ArrayList<ReportsEntity>();
        String hql ="select r  from UsersEntity as u inner join u.userReports as r\n" +
                "where u.userId=r.userIdReports and u.login=:login and r.isDeleted=0";
        Locale.setDefault(Locale.UK);
        reportsEntities=getAllEntityForUser(hql, login);
        return reportsEntities;
    }

    @Override
    public boolean updateEntity(MyEntity entity) {
        int result=0;
        ReportsEntity re = (ReportsEntity)entity;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        String hql = "update ReportsEntity set status=1, reportResult=:result where reportId=:id";
        try {
            transaction=session.beginTransaction();
            Query query = session.createQuery(hql);
            query.setParameter("result", re.getReportResult());
            query.setParameter("id", re.getReportId());
            result = query.executeUpdate();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null)
                transaction.rollback();
            e.printStackTrace();
            throw e;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        if (result>0) {
            return true;
        } else return false;
    }
}
