package com.rpgen.core.hbnt.dao;

import com.rpgen.core.hbnt.entities.MyEntity;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by denis on 08.04.16.
 *
 */
public abstract class EntityDAO {

    private SessionFactory sessionFactory;
    private Integer id = 0;

    public EntityDAO() {
        Locale.setDefault(Locale.UK);
    }

    public boolean createEntity(MyEntity entity) {
        Session session = null;
        session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        if (entity != null) {
            try {
                transaction = session.beginTransaction();
                id = (Integer) session.save(entity);
                transaction.commit();
            } catch (Exception e) {
                if (transaction != null)
                    transaction.rollback();
                e.printStackTrace();
                throw e;
            } finally {
                if (session != null) {
                    session.close();
                }
            }
        }
        if (id == 0) {
            return false;
        } else return true;
    }

    public Integer getId() {
        return id;
    }

    public MyEntity getEntity(int id) {
        List<MyEntity> connections = new ArrayList<MyEntity>();

        try {
            StringBuilder sb = new StringBuilder();
            if (!getTableName().equals("UsersEntity")) {
                sb.append("from ").append(getTableName()).append(" where id=:id and isDeleted=0");
            } else {
                sb.append("from ").append(getTableName()).append(" where id=:id");
            }
            Query query = HibernateUtil.getSessionFactory().openSession().createQuery(sb.toString());
            query.setParameter("id", id);
            connections = query.list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (connections.size() > 0) {
            return connections.get(0);
        } else {
            return null; //goes here, if connection does not exist
        }
    }

    public boolean updateEntity(MyEntity entity) {
        boolean updateEntity = false;
        Session session;
        session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        if (entity != null) {
            try {
                transaction = session.beginTransaction();
                session.update(entity);
                transaction.commit();
                updateEntity = true;
            } catch (Exception e) {
                if (transaction != null)
                    transaction.rollback();
                e.printStackTrace();
                throw e;
            } finally {
                if (session != null) {
                    session.close();
                }
            }
        }
        return updateEntity;
    }


    protected String getTableName() {
        return "";
    }

    public boolean deleteEntity(int id) {
        int result = 0;
        StringBuilder hql = new StringBuilder();
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        hql.append("update ").append(getTableName()).append(" set isDeleted = 1 where id=:id");
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery(hql.toString());
            query.setParameter("id", id);
            result += query.executeUpdate();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;
        } finally {
            session.close();
        }
        if (result > 0) {
            return true;
        } else {
            return false;
        }
    }

    protected List getAllEntityForUser(String hql, String login) {
        List<?> list = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Query query = session.createQuery(hql);
            query.setParameter("login", login);
            list = query.list();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return list;
    }


}
