package com.rpgen.core.ejb;


import org.json.JSONObject;
import org.json.JSONArray;
import com.rpgen.core.hbnt.entities.DbConnectionsEntity;
import com.rpgen.core.hbnt.entities.MyEntity;
import com.rpgen.core.hbnt.entities.ReportParamsEntity;
import com.rpgen.core.hbnt.entities.TemplatesEntity;
import com.rpgen.core.model.MyModel;
import com.rpgen.core.model.ReportParams;
import com.rpgen.core.model.Template;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Никита on 09.04.2016.
 */
public class ReportParamsBean  {


    public List<String> identifyParamsFromQuery(String query){
        List<String> paramsList = new ArrayList<>();
        String parameterIdentifier = ":[mM][yY][pP][aA][rR][aA][mM][0-9]*";
        Pattern pattern = Pattern.compile(parameterIdentifier);
        Matcher matcher = pattern.matcher(query);
        while (matcher.find()) {
            StringBuilder sb=new StringBuilder(matcher.group());
            sb.deleteCharAt(0);
            paramsList.add(sb.toString());
        }
        return paramsList;
    }
}
