package com.rpgen.core.ejb;

import com.rpgen.core.externdb.MyDB;
import com.rpgen.core.externdb.MySql;
import com.rpgen.core.externdb.Oracle;
import com.rpgen.core.hbnt.dao.ConnectionDAO;
import com.rpgen.core.hbnt.dao.DAOFactory;
import com.rpgen.core.hbnt.dao.EntityDAO;
import com.rpgen.core.hbnt.entities.DbConnectionsEntity;
import com.rpgen.core.hbnt.entities.MyEntity;
import com.rpgen.core.hbnt.entities.UsersEntity;
import com.rpgen.core.model.Connection;
import com.rpgen.core.model.MyModel;
import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.ejb.Singleton;
import javax.inject.Inject;

import static com.rpgen.core.HTTPResponseCodes.*;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by denis on 09.04.16.
 */

@Singleton
public class ConnectionBean extends MyBean {

    public ConnectionBean() {
    }

    @Inject
    private MyDB myDB;

    private static final Logger logger = Logger.getLogger(ConnectionBean.class);


    @Override
    protected MyEntity getEntityFromModel(MyModel model) {
        Connection connection = (Connection) model;
        DbConnectionsEntity connectionsEntity = new DbConnectionsEntity();
        if (connection.getConnectionId() != 0) {
            connectionsEntity.setConnectionId(connection.getConnectionId());
        }
        connectionsEntity.setConnectionName(connection.getConnectionName());
        connectionsEntity.setUserIdConnection((UsersEntity) DAOFactory.getInstance().getUserDAO().getEntity(connection.getUserId()));
        connectionsEntity.setConnectionDate(getCurrentDate());
        connectionsEntity.setDbType(connection.getdBType());
        connectionsEntity.setDbUsername(connection.getdBUsername());
        connectionsEntity.setDbPassword(connection.getdBPassword());
        connectionsEntity.setHostIp(getHostIp(connection.getHost()));
        connectionsEntity.setPort(connection.getPort());
        connectionsEntity.setSid(connection.getSid());
        connectionsEntity.setdBName(connection.getdBName());
        connectionsEntity.setIsDeleted(connection.getIsDeleted());
        if (connectionsEntity.getHostIp() == null) {
            connectionsEntity = null;
        }
        return connectionsEntity;
    }

    /**
     * get settings for a connection by connection id
     *
     * @param id
     * @return
     */
    public Connection getConnection( int id) {
        DbConnectionsEntity connection = (DbConnectionsEntity) DAOFactory.getInstance().getConnectionDAO().getEntity(id);
        Connection result = null;
        if (connection != null  ) {
            return  convertConnnectionsEntityToConnection(connection);
        } else {
            return null;
        }
    }


    public int update(Connection connection) {
        DbConnectionsEntity dbConnectionsEntity = (DbConnectionsEntity) getEntityFromModel(connection);
        boolean result = DAOFactory.getInstance().getConnectionDAO().updateEntity(dbConnectionsEntity);
        if (result)
            return OK;
        else
            return INTERNAL_ERROR;
    }

    public List<Connection> getAllConnections(String login) {
        List<DbConnectionsEntity> dbConnectionsEntities = DAOFactory.getInstance().getConnectionDAO().getAllConnections(login);
        List<Connection> connectionList = new ArrayList<Connection>(dbConnectionsEntities.size());
        Connection connection = new Connection();
        try {
            for (int i = 0; i < dbConnectionsEntities.size(); i++) {
                connection = convertConnnectionsEntityToConnection(dbConnectionsEntities.get(i));
                connectionList.add(i, connection);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connectionList;
    }


    @Override
    public int create(MyModel model) {
        Connection connection = (Connection) model;
        boolean result = false;
        logger.info("Creating new connection for user with ID " + connection.getUserId());
        try {
            result = DAOFactory.getInstance().getConnectionDAO().createEntity(getEntityFromModel(model));
            logger.info("Connection succesfully created");
        } catch (Exception e) {
            logger.error("When a new connection for user creating error occurred", e);
        }
        if (result){
              return OK;
        } else {
            return INTERNAL_ERROR;
        }
    }

    public boolean testConnection(Connection connection) {
        return myDB.testConnection(getSqlConnection(connection));
    }

    public java.sql.Connection getSqlConnection(Connection connection) {
        return myDB.getSqlConnection(connection);
    }


    private String getHostIp(String host) {

        int dotNum = 0;
        for (char c : host.toCharArray()) {
            if (c == '.') {
                dotNum++;
            }
        }
        if (dotNum != 3) {
            try {
                InetAddress address = InetAddress.getByName(host);
                host = address.getHostAddress();
            } catch (UnknownHostException e) {
                e.printStackTrace();
                logger.error("Unknown Host for DB connection", e);
            }
        }

        Pattern pattern = Pattern.compile("((25[0-5]|2[0-4]\\d|[01]?\\d\\d?)\\.){3}(25[0-5]|2[0-4]\\d|[01]?\\d\\d?)");
        Matcher matcher = pattern.matcher(host);
        if (matcher.matches()) {
            return host;
        } else return null;

    }

    public int deleteConnection(int id) {
        boolean result = false;
        try {
            result = DAOFactory.getInstance().getConnectionDAO().deleteEntity(id);
        } catch (Exception e) {
            logger.error("When a new connection with id " + id + "delete error occurred", e);
        }
        if (result) {
            return OK;
        } else {
            return INTERNAL_ERROR;
        }
    }


    private Connection convertConnnectionsEntityToConnection(DbConnectionsEntity dbConnectionsEntities) {
        Connection connection = new Connection();
        connection.setUserId(dbConnectionsEntities.getUserIdConnection().getUserId());
        connection.setConnectionId(dbConnectionsEntities.getConnectionId());
        connection.setConnectionName(dbConnectionsEntities.getConnectionName());
        connection.setConnectionDate(dbConnectionsEntities.getConnectionDate());
        connection.setdBType(dbConnectionsEntities.getDbType());
        connection.setdBUsername(dbConnectionsEntities.getDbUsername());
        connection.setdBPassword(dbConnectionsEntities.getDbPassword());
        connection.setHost(dbConnectionsEntities.getHostIp());
        connection.setPort(dbConnectionsEntities.getPort());
        connection.setSid(dbConnectionsEntities.getSid());
        connection.setdBName(dbConnectionsEntities.getdBName());
        return connection;
    }
}
