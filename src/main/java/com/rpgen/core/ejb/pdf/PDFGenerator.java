package com.rpgen.core.ejb.pdf;


import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.*;
import com.rpgen.core.ejb.ReportBean;
import com.rpgen.core.ejb.UserBean;
import com.rpgen.core.model.Report;
import com.rpgen.core.model.User;
import org.apache.log4j.Logger;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.awt.*;
import java.io.*;


/**
 * Created by denis on 16.04.16.
 *
 */
@Singleton
public class PDFGenerator {

    private static final Logger logger = Logger.getLogger(PDFGenerator.class);
    public File getPDF(String sessionlogin, int id) throws IOException {
        User user = new UserBean().getUser(sessionlogin);
        Report report = new ReportBean().getReport(sessionlogin,id);
        if (user.getUserId()==report.getUserID()) {
            Byte[] reportInByteObject = report.getResultReport();
            byte[] reportInBytePrim = new byte[reportInByteObject.length];
            int i = 0;
            for (Byte o : reportInByteObject) {
                reportInBytePrim[i++] = o;
            }
            JsonObject jo = deserialize(reportInBytePrim);

            //if (jo.getJsonArray("rows").size() != 0) {
                File file = null;
                try {
                    file = createPDF(report, jo);
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error("Error creating PDF", e);
                }
                logger.info("PDF for report with id " + report.getReportId() + " succesfully created");
                return file;
            //}
        } return null;
    }


    private File createPDF(Report report, JsonObject jsonObject)
        throws DocumentException, IOException {
            // step 1
        File file = File.createTempFile("report",".pdf");
        file.deleteOnExit();
        FileOutputStream fileout = new FileOutputStream(file);
        Document document = new Document(PageSize.A4.rotate(),0,0,0,0);
        document.setMargins(16, 14, 14, 14);
        // step 2
        PdfWriter writer=PdfWriter.getInstance(document, fileout);
        // step 3
        document.open();
        Paragraph title1 = new Paragraph("Name:"+report.getReportName(), FontFactory.getFont(FontFactory.HELVETICA, 16, Font.NORMAL, Color.black));
        Chapter chapter1 = new Chapter(title1, 1);
        chapter1.setNumberDepth(0);
        if (report.getReportDescription()!=null) {
            Paragraph text = new Paragraph("Description:"+report.getReportDescription(), FontFactory.getFont(FontFactory.HELVETICA, 14, Font.NORMAL, Color.black));
            chapter1.add(text);
        }
        Paragraph data = new Paragraph("Data creating a report:"+report.getReportDate(), FontFactory.getFont(FontFactory.HELVETICA, 14, Font.NORMAL, Color.black));
        chapter1.add(data);

         //change on rows
        if (jsonObject.getJsonArray("rows").size() != 0) {
            PdfPTable table = createTable(document, writer, jsonObject);
            chapter1.add(table);
        } else {
            Paragraph text = new Paragraph("Unfortunately, your request has no data in the database" , FontFactory.getFont(FontFactory.HELVETICA, 14, Font.NORMAL, Color.black));
            text.setAlignment(Element.ALIGN_CENTER);
            chapter1.add(text);
        }

        // step 4
            document.add(chapter1);
            // step 5
            document.close();
        return file;
    }

    private PdfPTable createTable( Document document, PdfWriter writer, JsonObject jsonObject) throws DocumentException, IOException {
        JsonArray rows = jsonObject.getJsonArray("rows");
        PdfPTable t = new PdfPTable(rows.getJsonArray(0).size());
        t.setSpacingBefore(25);

        t.setHorizontalAlignment(Element.ALIGN_CENTER);
        PdfPCell cell = null;
        Phrase phrase=null;
        BaseFont bf = BaseFont.createFont();
        float size=0;
        int k=0;
        float[]  widths=new float[rows.getJsonArray(0).size()];
        for (int i=0;i<rows.size();i++) {
            for (; k < rows.getJsonArray(i).size();) {
                if (rows.getJsonArray(i).get(k).toString().length() > size) {
                    widths[k] = bf.getWidthPoint(rows.getJsonArray(i).get(k).toString().toUpperCase(), 12);
                    size = rows.getJsonArray(i).get(k).toString().length();
                }
                    if (i!=rows.size()-1) {
                        break;
                    } else {
                        i=-1;
                        size=0;
                        k++;
                        break;
                    }
            }
        }
        t.setHeaderRows(1);
        t.setWidths(widths);
        t.setWidthPercentage(100);
        Font font=null;
        if (rows.getJsonArray(0).size()<=4) {
             font = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL, Color.black);
        }
        if ((rows.getJsonArray(0).size()>4) && (rows.getJsonArray(0).size()<=8)){
             font = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL, Color.black);
        }
        if (rows.getJsonArray(0).size()>8){
             font = FontFactory.getFont(FontFactory.HELVETICA, 6, Font.NORMAL, Color.black);
        }
        if (rows.getJsonArray(0).size()>16){
            font = FontFactory.getFont(FontFactory.HELVETICA, 5, Font.NORMAL, Color.black);
        }

        for (int i=0;i<rows.size();i++){
            for (int j=0;j<rows.getJsonArray(i).size();j++){
                if (i==0) {
                    phrase = new Phrase((rows.getJsonArray(i).get(j).toString().substring(1, rows.getJsonArray(i).get(j).toString().length()-1)),
                            font);
                } else {
                     phrase = new Phrase((rows.getJsonArray(i).get(j).toString().substring(1, rows.getJsonArray(i).get(j).toString().length() - 1)),
                            font);
                }
                cell = new PdfPCell(phrase);
                t.addCell(cell);
            }

        }
        return t;
    }


    private JsonObject deserialize(byte[] content) throws IOException {
        try (ByteArrayInputStream bais = new ByteArrayInputStream(content);
             JsonReader reader = Json.createReader(bais)) {
            return reader.readObject();
        }
    }

}
