package com.rpgen.core.ejb;

import com.rpgen.core.externdb.MyDB;
import com.rpgen.core.externdb.MySql;
import com.rpgen.core.externdb.Oracle;
import com.rpgen.core.externdb.PostgreSql;
import com.rpgen.core.hbnt.dao.DAOFactory;
import com.rpgen.core.hbnt.entities.MyEntity;
import com.rpgen.core.hbnt.entities.ReportsEntity;
import com.rpgen.core.hbnt.entities.TemplatesEntity;
import com.rpgen.core.hbnt.entities.UsersEntity;
import com.rpgen.core.model.*;
import com.rpgen.core.model.Connection;
import com.rpgen.core.model.Report;
import com.rpgen.core.model.ReportParams;
import com.rpgen.core.model.Template;
import com.rpgen.core.service.ReportControllerService;
import com.rpgen.mvc.model.*;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.ejb.Singleton;
import javax.inject.Inject;
import java.sql.*;
import java.util.*;

import static com.rpgen.core.HTTPResponseCodes.*;

/**
 * Created by denis on 09.04.16.
 *
 */
@Singleton
public class ReportBean extends MyBean {

    @Inject
    private ConnectionBean connectionBean;
    @Inject
    private TemplateBean templateBean;
    @Inject
    private MyDB myDB;


    private static final Logger logger = Logger.getLogger(ReportBean.class);

    @Override
    protected MyEntity getEntityFromModel(MyModel model) {
        Report report = (Report) model;
        ReportsEntity reportsEntity = new ReportsEntity();
        reportsEntity.setUserIdReports((UsersEntity) DAOFactory.getInstance().getUserDAO().getEntity(report.getUserID()));
        reportsEntity.setTemplatesEntity((TemplatesEntity) DAOFactory.getInstance().getTemplateDAO().getEntity(report.getTemplateID()));
        reportsEntity.setReportName(report.getReportName());
        reportsEntity.setDescription(report.getReportDescription());
        reportsEntity.setDateReport(getCurrentDate());
        reportsEntity.setIsDeleted(report.getIsDeleted());
        reportsEntity.setStatus(report.getStatus());
        reportsEntity.setReportResult(report.getResultReport());
        reportsEntity.setReportId(report.getReportId());
        return reportsEntity;
    }


    public Report getReport(String sessionUsername, int id) {
        UsersEntity usersEntity = DAOFactory.getInstance().getUserDAO().findUserByLogin(sessionUsername);
        ReportsEntity reportsEntity = (ReportsEntity) DAOFactory.getInstance().getReportDAO().getEntity(id);
        if (usersEntity.getUserId().equals(reportsEntity.getUserIdReports().getUserId())) {
            Report report = null;
            if (reportsEntity != null) {
                report = converReportEntityToReport(reportsEntity);
            }
            return report;
        } else return null;
    }

    public int deleteReport(int id) {
        boolean result = DAOFactory.getInstance().getReportDAO().deleteEntity(id);
        if (result) {
            return OK;
        } else {
            return INTERNAL_ERROR;
        }
    }

    @Override
    public int create(MyModel model) {
        logger.info("Creating new report...");
        try {
            DAOFactory.getInstance().getReportDAO().createEntity(getEntityFromModel(model));
        } catch (Exception e) {
            logger.error("When a new report creating error occurred ", e);
        }
        List<String> queryAndDBParam = queryAndDBParam(DAOFactory.getInstance().getReportDAO().getId(),((Report) model).getReportValues());
        ReportControllerService rp = new ReportControllerService();
        boolean resultOfSending = false;
        try {
            resultOfSending = rp.queue1(queryAndDBParam);
        } catch (Exception e) {
            logger.error("When sending data to refer to the database error occurred", e);
        }
        if (resultOfSending) {
            return OK;
        } else {
            return INTERNAL_ERROR;
        }
    }


    /**
     * return all report for user
     *
     * @return
     */

    public List<Report> getAllReports(String login) {
        List<ReportsEntity> reportsEntities = DAOFactory.getInstance().getReportDAO().getAllReports(login);
        List<Report> reportList = new ArrayList<Report>(reportsEntities.size());
        Report report = new Report();
        if (!reportsEntities.isEmpty()) {
            for (int i = 0; i < reportsEntities.size(); i++) {
                report = converReportEntityToReport(reportsEntities.get(i));
                reportList.add(i, report);
            }
        }
        return reportList;
    }

    private Report converReportEntityToReport(ReportsEntity reportsEntity) {
        Report report = new Report();
        report.setTemplateID(reportsEntity.getTemplatesEntity().getTemplateId());
        report.setReportDate(reportsEntity.getDateReport());
        report.setReportDescription(reportsEntity.getDescription());
        report.setReportName( reportsEntity.getReportName());
        report.setUserID(reportsEntity.getUserIdReports().getUserId());
        report.setReportId(reportsEntity.getReportId());
        report.setStatus(reportsEntity.getStatus());
        report.setResultReport(reportsEntity.getReportResult());
        return report;
    }

    public int update(int reportId, byte[] json) {
        Byte[] result = new Byte[json.length];
        int i = 0;
        for (byte o : json) {
            result[i++] = o;
        }
        ReportsEntity re = (ReportsEntity) DAOFactory.getInstance().getReportDAO().getEntity(reportId);
        Report report = converReportEntityToReport(re);
        report.setStatus(1);
        report.setResultReport(result);
        boolean resultOfUpdate = false;
        try {
            resultOfUpdate = DAOFactory.getInstance().getReportDAO().updateEntity(getEntityFromModel(report));
        } catch (Exception e) {
            logger.error("When writing the report results in a database error occurred", e);
        }
        if (resultOfUpdate) {
            return OK;
        } else {
            return INTERNAL_ERROR;
        }
    }

    /**
     * get string query with value of params
     *
     * @param report_id
     * @return list with final query and params to connect to DB
     */
    public List<String> queryAndDBParam(int report_id,Map<Integer,String> paramValues) {
        List<String> queryAndDBParam = new ArrayList<>();
        ReportsEntity re = (ReportsEntity) DAOFactory.getInstance().getReportDAO().getEntity(report_id);
        Report report = converReportEntityToReport(re);
        Template template = templateBean.getTemplate(report.getTemplateID());
        Connection connection = connectionBean.getConnection(template.getConnctionID());

        String templateQuery = template.getTemplateQuery();

        for (Map.Entry<Integer,String> pairs:paramValues.entrySet()) {
            int paramId = pairs.getKey();

            String paramValue = pairs.getValue();
            Object[] objects = template.getReportParams().toArray();
            for(Object o:objects) {
                ReportParams reportParams = (ReportParams) o;
                if (reportParams.getParamId()==paramId) {
                    if (reportParams.getTypeVarParam().equalsIgnoreCase("text"))
                        templateQuery=templateQuery.replaceAll(":"+reportParams.getVarParam(),'\''+paramValue+'\'');
                    else
                        templateQuery=templateQuery.replaceAll(":"+reportParams.getVarParam(),paramValue);
                }
            }


        }

        String stringAccess;

        switch (connection.getdBType().toLowerCase()) {
            case "oracle":
                stringAccess = new Oracle().stringAccess(connection);
                break;
            case "mysql":
                stringAccess = new MySql().stringAccess(connection);
                break;
            case "postgresql":
                stringAccess=new PostgreSql().stringAccess(connection);
                break;
            default:
                stringAccess = "";
        }

        queryAndDBParam.add(templateQuery);
        queryAndDBParam.add(String.valueOf(report_id));
        queryAndDBParam.add(stringAccess);
        queryAndDBParam.add(connection.getdBType());
        queryAndDBParam.add(connection.getdBUsername());
        queryAndDBParam.add(connection.getdBPassword());

        myDB.testConnection(connectionBean.getSqlConnection(connection));

        return queryAndDBParam;
    }
}
