package com.rpgen.core.ejb;

import com.rpgen.core.hbnt.dao.*;
import com.rpgen.core.hbnt.entities.MyEntity;
import com.rpgen.core.model.MyModel;
import com.rpgen.core.model.ReportParams;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by denis on 09.04.16.
 */
public abstract class MyBean {


    public abstract int create(MyModel model);

    protected abstract MyEntity getEntityFromModel(MyModel model);


    protected Date getCurrentDate(){
        Date date;
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        date = cal.getTime();
        return date;
    }
}
