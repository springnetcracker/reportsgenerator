package com.rpgen.core.ejb;

import com.rpgen.core.externdb.MyDB;
import com.rpgen.core.externdb.Oracle;
import com.rpgen.core.hbnt.dao.DAOFactory;
import com.rpgen.core.hbnt.entities.*;
import com.rpgen.core.model.*;
import com.rpgen.core.model.Connection;
import org.apache.log4j.Logger;

import javax.ejb.Singleton;
import javax.inject.Inject;
import java.sql.*;
import java.util.*;
import java.util.regex.Pattern;

import static com.rpgen.core.HTTPResponseCodes.*;

/**
 * Created by denis on 09.04.16.
 *
 */
@Singleton
public class TemplateBean extends MyBean{

    @Inject
    private ConnectionBean connectionBean;

    @Inject
    private UserBean userBean;

    public Oracle oracle;

    private static final Logger logger = Logger.getLogger(TemplateBean.class);

    @Override
    protected MyEntity getEntityFromModel(MyModel model) {
        Template template=(Template)model;
        TemplatesEntity templatesEntity = new TemplatesEntity();
        templatesEntity.setDbConnectionsEntity((DbConnectionsEntity) DAOFactory.getInstance().getConnectionDAO().getEntity(template.getConnctionID()));
        templatesEntity.setTemplateName(template.getTemplateName());
        templatesEntity.setTemplateDate(getCurrentDate());
        templatesEntity.setQuery(template.getTemplateQuery());
        templatesEntity.setIsDeleted(template.getIsDeleted());
        templatesEntity.setTemplateId(template.getTemplateId());
        if (template.getReportParams()==null){
            return templatesEntity;
        }
        for (ReportParams reportParams:template.getReportParams()) {
            try {
                ReportParamsEntity reportParamsEntity = new ReportParamsEntity();
                reportParamsEntity.setParamName(reportParams.getParamName());
                reportParamsEntity.setTemplateId(templatesEntity);
                reportParamsEntity.setTypeVarParam(reportParams.getTypeVarParam());
                reportParamsEntity.setVarParam(reportParams.getVarParam());
                templatesEntity.addParam(reportParamsEntity);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        return templatesEntity;
    }


    public int create(MyModel model) {
        Template template= (Template)model;
        boolean result=false;
        logger.info("Creating new template fro connection "+template.getConnctionID());
        try {
            result = DAOFactory.getInstance().getTemplateDAO().createEntity(getEntityFromModel(model));
        } catch (Exception e){
            logger.error("When a new template for "+template.getConnctionID()+" creating error occurred", e);
        }
        if (result){
            logger.info("New template successfully created ");
            return OK;
        } else {
            return INTERNAL_ERROR;
        }
    }


    public int deleteTemplate(int id){
        boolean result = DAOFactory.getInstance().getTemplateDAO().deleteEntity(id);
        if (result){
            return OK;
        } else {
            return  INTERNAL_ERROR;
        }
    }

    public Template getTemplate(int id){

        TemplatesEntity templatesEntity = (TemplatesEntity) DAOFactory.getInstance().getTemplateDAO().getEntity(id);
        Template template=null;
        if (templatesEntity!=null){
            template= convertTemplateEntityToTemplate(templatesEntity);
        } return template;
    }

    public List<Template> getAllTemplatesByConnection(String sessionUsename, int connection_id){
        UsersEntity user = DAOFactory.getInstance().getUserDAO().findUserByLogin(sessionUsename);
        DbConnectionsEntity connection = (DbConnectionsEntity)DAOFactory.getInstance().getConnectionDAO().getEntity(connection_id);
        if (connection.getUserIdConnection().getUserId()==user.getUserId()) {
            List<TemplatesEntity> templatesEntities = DAOFactory.getInstance().getTemplateDAO().getAllTemplatesByConnection(connection_id);
            List<Template> templateList = new ArrayList<Template>(templatesEntities.size());
            Template template = null;
            if (!templatesEntities.isEmpty()) {
                for (int i = 0; i < templatesEntities.size(); i++) {
                    template = convertTemplateEntityToTemplate(templatesEntities.get(i));
                    templateList.add(i, template);
                }
            }
            return templateList;
        } else {
            return null;
        }
    }


    public List<Template> getAllTemplatesByUser(String login){
        //String login = userDAO.findUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
        List<TemplatesEntity> templatesEntities = DAOFactory.getInstance().getTemplateDAO().getAllTemplatesByUser(login);
        List<Template> templateList = new ArrayList<Template>(templatesEntities.size());
        Template template =null;
        if (!templatesEntities.isEmpty()) {
            for (int i = 0; i < templatesEntities.size(); i++) {
                template=convertTemplateEntityToTemplate(templatesEntities.get(i));
                templateList.add(i, template);
            }
        }
        return templateList;
    }

    private Template convertTemplateEntityToTemplate(TemplatesEntity templatesEntity){
        Template template= new Template();
        template.setConnctionID(templatesEntity.getDbConnectionsEntity().getConnectionId());
        template.setTemplateName(templatesEntity.getTemplateName());
        template.setTemplateDate(templatesEntity.getTemplateDate());
        template.setTemplateQuery(templatesEntity.getQuery());
        template.setTemplateId(templatesEntity.getTemplateId());
        Set<ReportParams> reportParamsHashSet = new HashSet<>();
        for (ReportParamsEntity te:templatesEntity.getReportParamsEntitySet()){
            ReportParams reportParams=new ReportParams();
            reportParams.setParamId(te.getParamId());
            reportParams.setParamName(te.getParamName());
            reportParams.setVarParam(te.getVarParam());
            reportParams.setTypeVarParam(te.getTypeVarParam());
            reportParams.setTemplateId(te.getTemplateId().getTemplateId());
            reportParamsHashSet.add(reportParams);
        }
        template.setReportParams(reportParamsHashSet);
        return template;
    }

    /**
     * check valid query
     *
     * @param template
     * @return
     */
    public String checkQuery(Template template) {
        Connection connection = connectionBean.getConnection(template.getConnctionID());
        java.sql.Connection sqlConnection = new MyDB().getSqlConnection(connection);
        //String[] selectGroup = oracle.selectGroup;

        Set<ReportParams> reportParamsSet = template.getReportParams();
        String templateQuery = template.getTemplateQuery();

        for (ReportParams reportParams:reportParamsSet) {
            String paramName = ":"+reportParams.getVarParam();
            if (templateQuery.contains(paramName)){
                templateQuery=templateQuery.replaceAll(paramName,"null");
            }
        }

        /*String query = buildQueryFromParams(template);

        String queryCopy = query.toLowerCase();
        switch (connection.getdBType().toLowerCase()) {
            case "oracle":
                if (queryCopy.contains("where")) {
                    int indexWhere = query.indexOf("where");
                    queryCopy = query.substring(indexWhere,indexWhere+5);
                    query = query.replaceFirst(queryCopy,"WHERE ROWNUM < 2 and");
                } else {
                    int indexWhere = query.length();
                    String[] selectGroup2 = new Oracle().selectGroup;
                    for (String string:selectGroup2) {
                        if (queryCopy.contains(string)) {
                            indexWhere = queryCopy.indexOf(string);
                            break;
                        }
                    }
                    query = query.substring(0,indexWhere) +
                            " WHERE  ROWNUM < 2 " + query.substring(indexWhere,queryCopy.length());
                }
                break;
            case "mysql":
                if (queryCopy.contains("procedure")) {
                    query = query.substring(0,queryCopy.indexOf("procedure")) +
                            "limit 1 " + query.substring(queryCopy.indexOf("procedure"),queryCopy.length());
                } else
                    query += " limit 1";
                break;
            default:
        }*/

        Statement stmt;
        ResultSet rs = null;
        try {

            stmt = sqlConnection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(templateQuery);

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("{");
            stringBuilder.append("\"result\":\"Valid\"");
            stringBuilder.append("}");
            return stringBuilder.toString();
        } catch (SQLException e) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("{");
            String errorSql = e.getMessage().replaceAll("\"","\'").replaceAll("\n","");
            stringBuilder.append("\"result\":\"" + errorSql + "\"");
            stringBuilder.append("}");
            return stringBuilder.toString();

        } catch (Exception e) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("{");
            stringBuilder.append("\"result\":\"Syntax error\"");
            stringBuilder.append("}");
            return stringBuilder.toString();
        }

    }

    private String buildQueryFromParams(Template template) {


        Set<ReportParams> reportParamsSet = template.getReportParams();
        String templateQuery = template.getTemplateQuery();

        for (ReportParams reportParams:reportParamsSet) {
            String paramName = ":"+reportParams.getParamName();
            String paramValue = reportParams.getVarParam();
            if (templateQuery.contains(paramName)){
                templateQuery=templateQuery.replaceAll(paramName,paramValue);
            }
        }

        return templateQuery;
    }

}

