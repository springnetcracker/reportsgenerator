package com.rpgen.core.ejb;

import com.rpgen.core.hbnt.dao.DAOFactory;
import com.rpgen.core.hbnt.entities.MyEntity;
import com.rpgen.core.hbnt.entities.UserRolesEntity;
import com.rpgen.core.hbnt.entities.UsersEntity;
import com.rpgen.core.model.MyModel;
import com.rpgen.core.model.User;
import org.apache.log4j.Logger;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.ejb.Singleton;

import static com.rpgen.core.HTTPResponseCodes.*;

/**
 * Created by denis on 09.04.16.
 */
@Singleton
public class UserBean extends MyBean{

    private static final Logger logger = Logger.getLogger(UserBean.class);

    @Override
    protected MyEntity getEntityFromModel(MyModel model) {
        User user=(User)model;
        UsersEntity usersEntity = new UsersEntity();
        usersEntity.setLogin(user.getLogin());
        usersEntity.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        usersEntity.setFirstName(user.getFirstName());
        usersEntity.setLastName(user.getLastName());
        usersEntity.setEmail(user.getEmail());
        UserRolesEntity role = new UserRolesEntity();
        role.setUser(usersEntity);
        role.setRoleId(2);
        usersEntity.addRole(role);
        return usersEntity;
    }


    @Override
    public int create(MyModel model) {
        User user =(User)model;
        boolean result=false;
        logger.info("Creating new user with login "+user.getLogin());
            try {
                 result= DAOFactory.getInstance().getUserDAO().createEntity(getEntityFromModel(model));
            } catch (Exception e){
                if (e.getCause().getMessage().equals("ORA-00001: unique constraint (COREDB.USERS_UK1) violated\n")){
                    logger.info("user with same login already exists");
                    return CONFLICT;
                }
                    logger.error("When a new user registration error occurred",e);
            }
            if (result){
                logger.info("User with login "+user.getLogin()+" succesfully created");
                return OK;
            } else {
                return INTERNAL_ERROR;
            }
    }

    public User getUser(String login){
        UsersEntity usersEntity = null;
        usersEntity= DAOFactory.getInstance().getUserDAO().findUserByLogin(login);
        User user=null;
        if (usersEntity!=null) {
            user=getUserFromEserEntity(usersEntity);
        } return user;
    }

    private User getUserFromEserEntity(UsersEntity usersEntity){
        User user = new User();
        user.setUserId(usersEntity.getUserId());
        user.setLogin(usersEntity.getLogin());
        user.setFirstName(usersEntity.getFirstName());
        user.setLastName(usersEntity.getLastName());
        user.setEmail(usersEntity.getEmail());
        return user;
    }

}
