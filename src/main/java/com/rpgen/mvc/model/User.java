package com.rpgen.mvc.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class User {
    private String firstName, lastName, password, login, email;

    private int userId;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String name) {
        this.firstName = name;
    }

    public String getLastName () {return lastName;}

    public void setLastName (String lastName) {this.lastName = lastName;}

    public void setUserId (int id) {this.userId = id;}

    public int getUserId () {return userId;}

    public String getLogin () {return login;}

    public void setLogin (String login) {this.login = login;}

    public String getEmail () {return email;}

    public void setEmail (String email) {this.email = email;}

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /*@Override
    public String toString() {
        return "Quote{" +
                "firstName='" + firstName + '\'' +
                ", lastName" + lastName + '\'' +
                ", login=" + login + '\'' +
                ", password=" + password + '\'' +
                ", email=" + email + '\'' +
                '}';
    } */

}
