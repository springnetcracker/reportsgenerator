package com.rpgen.mvc.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class Template{
    private int connctionID;
    private String templateName;
    private int isDeleted;
    private Date templateDate;
    private String templateQuery;
    private int templateId;
    private Set<ReportParams> reportParams;

    public Template(){

    }

    public Template (int connctionID, String templateName, String templateQuery, Set<ReportParams> reportParams) {
        this.connctionID = connctionID;
        this.templateName = templateName;
        this.templateQuery = templateQuery;
        this.reportParams = reportParams;
    }

    public Template(int connctionID, String templateName, String templateQuery, Date templateDate, Set<ReportParams>reportParams, int templateId){
        this.connctionID=connctionID;
        this.templateName=templateName;
        this.templateDate=templateDate;
        this.templateQuery=templateQuery;
        this.reportParams=new HashSet<ReportParams>(reportParams);
        this.templateId=templateId;
        isDeleted=0;
    }

    public int getConnctionID() {
        return connctionID;
    }

    public void setConnctionID(int connctionID) {
        this.connctionID = connctionID;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public Date getTemplateDate() {
        return templateDate;
    }

    public void setTemplateDate(Date templateDate) {
        this.templateDate = templateDate;
    }

    public String getTemplateQuery() {
        return templateQuery;
    }

    public void setTemplateQuery(String templateQuery) {
        this.templateQuery = templateQuery;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public int getTemplateId() {
        return templateId;
    }

    public void setTemplateId(int templateId) {
        this.templateId = templateId;
    }

    public Set<ReportParams> getReportParams() {
        return reportParams;
    }

    public void setReportParams(Set<ReportParams> reportParams) {
        this.reportParams = reportParams;
    }
}
