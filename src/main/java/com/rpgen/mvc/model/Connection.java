package com.rpgen.mvc.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by Maria Lisunova on 19.04.2016.
 *
 */
@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class Connection{

    private int connectionId;
    private  int userId;
    private String connectionName;
    private String dBType;
    private Date connectionDate;
    private String host;
    private  String dBUsername;
    private  String dBPassword;
    private int port;
    private String sid;
    private String dBName;

    public Connection() {}

    public Connection (String connectionName, String dBType, String dBName, String dBUsername, String dBPassword, String host, int port, int userId, String sid) {
        this.connectionName = connectionName;
        this.dBType = dBType;
        this.dBName = dBName;
        this.dBUsername = dBUsername;
        this.dBPassword = dBPassword;
        this.host = host;
        this.userId = userId;
        this.port = port;
        this.sid = sid;
    }

    public int getConnectionId() {
        return connectionId;
    }

    public int getUserId() {
        return userId;
    }

    public String getConnectionName() {
        return connectionName;
    }

    public String getdBType() {
        return dBType;
    }

    public Date getConnectionDate() {
        return connectionDate;
    }

    public String getHost() {
        return host;
    }

    public String getdBUsername() {
        return dBUsername;
    }

    public String getdBPassword() {
        return dBPassword;
    }

    public int getPort() {
        return port;
    }

    public String getSid() {
        return sid;
    }

    public void setConnectionId(int connectionId) {
        this.connectionId = connectionId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setConnectionName(String connectionName) {
        this.connectionName = connectionName;
    }

    public void setdBType(String dBType) {
        this.dBType = dBType;
    }

    public void setConnectionDate(Date connectionDate) {
        this.connectionDate = connectionDate;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setdBUsername(String dBUsername) {
        this.dBUsername = dBUsername;
    }

    public void setdBPassword(String dBPassword) {
        this.dBPassword = dBPassword;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getdBName() {
        return dBName;
    }

    public void setdBName(String dBName) {
        this.dBName = dBName;
    }
}

