package com.rpgen.mvc.model;


public class ReportParams{
    private Integer templateId;
    private String paramName;
    private String varParam;
    private String typeVarParam;
    private int paramId;


    public ReportParams() {
    }

    public ReportParams(Integer templateId, String varParam, String paramName, String typeVarParam) {
        this.templateId = templateId;
        this.varParam = varParam;
        this.paramName = paramName;
        this.typeVarParam = typeVarParam;
    }

    public Integer getTemplateId() {
        return templateId;
    }


    public String getParamName() {
        return paramName;
    }

    public String getVarParam() {
        return varParam;
    }

    public String getTypeVarParam() {
        return typeVarParam;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public void setVarParam(String varParam) {
        this.varParam = varParam;
    }

    public void setTypeVarParam(String typeVarParam) {
        this.typeVarParam = typeVarParam;
    }

    public void setTemplateId(Integer templateId) {
        this.templateId = templateId;
    }

    public int getParamId() {
        return paramId;
    }

    public void setParamId(int paramId) {
        this.paramId = paramId;
    }
}
