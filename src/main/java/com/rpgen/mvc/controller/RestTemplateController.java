package com.rpgen.mvc.controller;

import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;



/**
 * Created by Maria Lisunova on 26.04.2016.
 *
 */
class RestTemplateController {
    private static RestTemplate rt = new RestTemplate();

    static ResponseEntity connectRest (String uri, HttpMethod httpMethod, Class objectClass) {
        String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", "JSESSIONID=" + sessionId);
        HttpEntity requestEntity = new HttpEntity(null, requestHeaders);

        RestTemplate rt = new RestTemplate();
        try {
            rt.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            return rt.exchange(uri, httpMethod, requestEntity, objectClass);

        }
        catch (HttpClientErrorException e) {
            throw e;
        }
    }

    static ResponseEntity connectRest (String uri, HttpMethod httpMethod, Class objectClass, Object object) {
        String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", "JSESSIONID=" + sessionId);
        HttpEntity requestEntity = new HttpEntity(object, requestHeaders);
        RestTemplate rt = new RestTemplate();
        try {
            rt.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            return rt.exchange(uri, httpMethod, requestEntity, objectClass);

        }
        catch (HttpClientErrorException e) {
            throw e;
        }
        catch (Exception e) {
            throw e;
        }
    }

    static void postForObject (String uri, Object obj) {
        rt.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
        rt.getMessageConverters().add(new StringHttpMessageConverter());
        try {
            rt.postForObject(uri, obj, ResponseEntity.class);
        }
        catch (HttpClientErrorException e) {
            throw e;
        }
    }

}
