package com.rpgen.mvc.controller;

import com.rpgen.mvc.UrlConstants;
import com.rpgen.mvc.model.Connection;
import com.rpgen.mvc.model.User;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;


/**
 * Created by Maria Lisunova on 05.04.2016.
 *
 */
@Controller
@RequestMapping(value = "/home")
@SessionAttributes("userJSP")
public class UserPageController {
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView returnUserPage () {
        ModelAndView modelAndView = new ModelAndView();
        User user;
        try {
            user = (User)RestTemplateController.connectRest(UrlConstants.GET_USER + getUserName(), HttpMethod.GET, User.class).getBody();
            modelAndView.addObject("userJSP", user);
            modelAndView.setViewName("userPage");
            return modelAndView;
        }
        catch (HttpClientErrorException e) {
            modelAndView.setViewName("redirect:/error");
            return modelAndView;
        }
    }

    private String getUserName () {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        return username;
    }
}
