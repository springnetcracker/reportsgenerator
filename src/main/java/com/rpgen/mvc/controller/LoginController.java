package com.rpgen.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Maria Lisunova on 05.04.2016.
 *
 */
@Controller
public class LoginController {
    @RequestMapping("/login")
    public ModelAndView returnLoginPage (@RequestParam(value = "error", required = false) String error ) {
        ModelAndView modelAndView = new ModelAndView();
        if (error != null) modelAndView.addObject("error", "Invalid username or password!");
        modelAndView.setViewName("login");
        return modelAndView;
    }

}
