package com.rpgen.mvc.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rpgen.mvc.UrlConstants;
import com.rpgen.mvc.model.Connection;
import com.rpgen.mvc.model.ReportParams;
import com.rpgen.mvc.model.Template;
import com.rpgen.mvc.model.User;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.ModelAndView;


import java.io.IOException;
import java.util.*;

/**
 * Created by Maria Lisunova on 20.04.2016.
 *
 */

@Controller
@RequestMapping("/template")
@SessionAttributes("userJSP")
public class TemplateController {
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView returnTemplatesPage (@ModelAttribute("userJSP") User user) {
        ModelAndView modelAndView = new ModelAndView();
        String templ = getAllTemplates(user.getLogin());
        if (templ != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                List<Template> templates = objectMapper.readValue(templ, new TypeReference<List<Template>>() {});
                modelAndView.addObject("templates", templates);
                modelAndView.addObject("userJSP", user);
                modelAndView.setViewName("template");
            } catch (HttpClientErrorException e) {
                e.printStackTrace();
            }
            catch (IOException e) {
                e.printStackTrace();
                modelAndView.setViewName("redirect:/error");
            }
        }
        return modelAndView;
    }

    @RequestMapping(value="create", method= RequestMethod.GET)
    public ModelAndView returnConnectionForm (@ModelAttribute("userJSP") User user) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("userJSP", user);
        String conns = getAllConnections(user.getLogin());
        if (conns != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                List<Connection> connections = objectMapper.readValue(conns, new TypeReference<List<Connection>>() {});
                modelAndView.addObject("connections", connections);
            }
            catch (IOException e) {
                modelAndView.setViewName("redirect:/error");
                e.printStackTrace();
            }
        }
        else modelAndView.addObject("connections", null);
        modelAndView.setViewName("addTemplate");
        return modelAndView;
    }

    @RequestMapping (value="create", method = RequestMethod.POST)
    public ModelAndView returnCreateTemplateInterface (Template template, @RequestParam("connectionName") String connectionName, @ModelAttribute("userJSP") User user, @RequestParam("paramNames") String [] paramNames, @RequestParam("paramVars") String [] paramVars, @RequestParam("paramTypes") String [] paramTypes) {
        ModelAndView modelAndView = new ModelAndView();
        Set<ReportParams> paramsSet = getReportParams(paramNames, paramVars, paramTypes);
        template.setReportParams(paramsSet);
        template.setConnctionID(getConnectionByName(connectionName, user.getLogin()));
        try {
            RestTemplateController.connectRest(UrlConstants.CREATE_TEMPLATE, HttpMethod.POST, ResponseEntity.class, template);
            modelAndView.setViewName("redirect:/template");
            return modelAndView;
        }
        catch (HttpClientErrorException e) {
            modelAndView.setViewName("redirect:/error");
            return modelAndView;
        }
    }

    @RequestMapping(value = "/{templateID}", method = RequestMethod.GET)
    public ModelAndView returnTemplateByID (@PathVariable("templateID") int templateID) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            Template foundTemplate = (Template)RestTemplateController.connectRest(UrlConstants.GET_TEMPLATE_BY_ID + templateID, HttpMethod.GET, Template.class).getBody();
            modelAndView.addObject("foundTemplate", foundTemplate);
            modelAndView.addObject("connectionName", getConnectionByID(foundTemplate.getConnctionID()));
            modelAndView.setViewName("templatePage");
            return modelAndView;
        }
        catch (HttpClientErrorException e) {
            modelAndView.setViewName("redirect:/error");
            e.printStackTrace();
            return modelAndView;
        }
    }

    @RequestMapping(value = "/delete/{templateID}")
    public ModelAndView deleteTemplate (@PathVariable("templateID") int templateID) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            RestTemplateController.connectRest(UrlConstants.GET_TEMPLATE_BY_ID + templateID, HttpMethod.DELETE, ResponseEntity.class);
            modelAndView.setViewName("redirect:/template");
        }
        catch (HttpClientErrorException e) {
            e.printStackTrace();
            modelAndView.setViewName("redirect:/error");
        }
        return modelAndView;
    }

    @RequestMapping(value = "/check", method = RequestMethod.POST)
    @ResponseBody
    public int checkSyntaxis (@ModelAttribute("userJSP") User user, @RequestParam("templateName") String templateName, @RequestParam("templateQuery") String templateQuery, @RequestParam("connectionName") String connectionName,  @RequestParam("paramNames") String [] paramNames, @RequestParam("paramVars") String [] paramVars, @RequestParam("paramTypes") String [] paramTypes) {
        Set <ReportParams> reportParams = getReportParams(paramNames, paramVars, paramTypes);
        Template template = new Template (getConnectionByName(connectionName, user.getLogin()), templateName, templateQuery, reportParams);
        try {
            return RestTemplateController.connectRest(UrlConstants.CHECK_SYNTAXIS, HttpMethod.POST, Template.class, template).getStatusCode().value();
        }
        catch (HttpClientErrorException e) {
            return -1;
        }
    }

    private int getConnectionByName (String connectionName, String username) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            List <Connection> connections = objectMapper.readValue(getAllConnections(username), new TypeReference<List<Connection>>(){});
            for (Connection connection: connections) {
                if (connection.getConnectionName().equals(connectionName)) return connection.getConnectionId();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

    private String getAllTemplates (String username) {
        String templates;
        try {
            templates = (String) RestTemplateController.connectRest(UrlConstants.GET_ALL_TEMPLATES + username, HttpMethod.GET, String.class).getBody();
        }
        catch (HttpClientErrorException e) {
            throw e;
        }
        return templates;
    }

    private String getAllConnections (String username) {
        String connections = "";
        try {
            connections = (String)RestTemplateController.connectRest(UrlConstants.GET_ALL_CONNECTIONS + username, HttpMethod.GET, String.class).getBody();
        }
        catch (HttpClientErrorException e) {
            throw e;
        }
        return connections;
    }

    private String getConnectionByID (int connectionID) {
        try {
            Connection foundConnection = (Connection)RestTemplateController.connectRest(UrlConstants.GET_CONNECTION_BY_ID + connectionID, HttpMethod.GET, Connection.class).getBody();
            return foundConnection.getConnectionName();
        }
        catch (HttpClientErrorException e) {
            throw e;
        }
    }

    private Set <ReportParams> getReportParams (String [] paramNames, String [] paramVars, String [] paramTypes ) {
        Set<ReportParams> paramsSet = new HashSet<>();
        for (int i = 0; i < paramVars.length; i++) {
            ReportParams reportParams = new ReportParams();
            reportParams.setParamName(paramNames[i]);
            reportParams.setVarParam(paramVars[i]);
            reportParams.setTypeVarParam(paramTypes[i]);
            paramsSet.add(reportParams);
        }
        return paramsSet;
    }
}
