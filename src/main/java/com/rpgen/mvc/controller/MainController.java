package com.rpgen.mvc.controller;

import com.rpgen.mvc.model.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import javax.servlet.http.HttpSession;


@Controller
public class MainController {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String main(HttpSession httpSession) {
        httpSession.setAttribute("userJSP", new User());
        return "index";
    }

    @RequestMapping("/error")
    public String errorPage () {
        return "error";
    }


}
