package com.rpgen.mvc.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rpgen.mvc.UrlConstants;
import com.rpgen.mvc.model.Report;
import com.rpgen.mvc.model.ReportParams;
import com.rpgen.mvc.model.Template;
import com.rpgen.mvc.model.User;
import com.sun.javafx.sg.prism.NGShape;
import org.apache.commons.io.IOUtils;
import org.springframework.http.*;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.*;


/**
 * Created by Maria Lisunova on 29.04.2016.
 *
 */

@Controller
@SessionAttributes("userJSP")
@RequestMapping("/report")
public class ReportController {
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getReportsPage(@ModelAttribute("userJSP") User user) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("userJSP", user);
        String reportsString = getAllReports(user.getLogin());
        if (reportsString != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                List<Report> reports = objectMapper.readValue(reportsString, new TypeReference<List<Report>>() {});
                modelAndView.addObject("reports", reports);
                modelAndView.setViewName("report");
            }
            catch (IOException e) {
                modelAndView.setViewName("redirect:/error");
            }
            catch (HttpClientErrorException e) {
                modelAndView.setViewName("redirect:/error");
            }
        }

        return modelAndView;
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView createReportPage(@ModelAttribute("userJSP") User user, HttpSession httpSession) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("userJSP", user);
        String templ = getAllTemplates(user.getLogin());
        if (templ != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                List<Template> templates = objectMapper.readValue(templ, new TypeReference<List<Template>>() {});
                httpSession.setAttribute("templateCollection", templates);
                modelAndView.addObject("templateCollection", templates);
            } catch (IOException e) {
                modelAndView.setViewName("redirect:/error");
            }
        }
        else {
            modelAndView.addObject("templates", null);
        }
        modelAndView.setViewName("addReport");
        return modelAndView;
    }

    @RequestMapping(value = "/create/next", method = RequestMethod.GET)
    public ModelAndView nextReportPage(HttpSession httpSession, @RequestParam("reportDescription") String reportDescription, @ModelAttribute("userJSP") User user, @RequestParam("reportName") String reportName, @RequestParam("templateName") String templateName) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("userJSP", user);
        Report report = new Report();
        report.setTemplateName(templateName);
        report.setReportDescription(reportDescription);
        List<Template> templates = (List<Template>) httpSession.getAttribute("templateCollection");
        Template chosenTemplate = new Template();
        for (Template template : templates) {
            if (template.getTemplateName().equals(templateName)) chosenTemplate = template;
        }
        report.setReportName(reportName);
        report.setUserID(user.getUserId());
        report.setTemplateID(chosenTemplate.getTemplateId());
        httpSession.setAttribute("report", report);
        modelAndView.addObject("report", report);
        httpSession.setAttribute("reportParameters", chosenTemplate.getReportParams());
        modelAndView.addObject("reportParameters", chosenTemplate.getReportParams());
        modelAndView.setViewName("reportValues");
        return modelAndView;

    }

    @RequestMapping(value = "/create/next", method = RequestMethod.POST)
    public ModelAndView nextReportPage(@ModelAttribute("userJSP") User user, HttpSession httpSession, String[] values) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("userJSP", user);
        Map<Integer, String> reportValues = new HashMap<>();
        HashSet<ReportParams> reportParams = (HashSet<ReportParams>) httpSession.getAttribute("reportParameters");
        Report report = (Report) httpSession.getAttribute("report");
        int count = 0;
        for (ReportParams rp : reportParams) {
            reportValues.put(rp.getParamId(), values[count]);
            count++;
        }
        report.setReportValues(reportValues);
        try {
            RestTemplateController.connectRest(UrlConstants.CREATE_REPORT, HttpMethod.POST, ResponseEntity.class, report);
            modelAndView.setViewName("redirect:/report");
            return modelAndView;
        } catch (HttpClientErrorException e) {
            modelAndView.setViewName("redirect:/error");
            return modelAndView;
        }
    }

    @RequestMapping(value = "/delete/{reportID}")
    public ModelAndView deleteReport(@PathVariable("reportID") int reportID) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            RestTemplateController.connectRest(UrlConstants.GET_REPORT_BY_ID + reportID, HttpMethod.DELETE, ResponseEntity.class);
            modelAndView.setViewName("redirect:/report");
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
            modelAndView.setViewName("redirect:/error");
        }
        return modelAndView;
    }

    @RequestMapping(value = "/{reportID}", method = RequestMethod.GET)
    public ModelAndView returnReportByID(@PathVariable("reportID") int reportID) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            Report foundReport = (Report) RestTemplateController.connectRest(UrlConstants.GET_REPORT_BY_ID + reportID, HttpMethod.GET, Report.class).getBody();
            modelAndView.addObject("foundReport", foundReport);
            modelAndView.addObject("template", getTemplateByID(foundReport.getTemplateID()));
            modelAndView.setViewName("reportPage");
            return modelAndView;
        } catch (HttpClientErrorException e) {
            modelAndView.setViewName("redirect:/error");
            e.printStackTrace();
            return modelAndView;
        }
    }

    @RequestMapping(value = "/download/{reportID}", method = RequestMethod.GET)
    public void downloadReport(@PathVariable("reportID") int reportID, HttpServletResponse httpServletResponse) {
        File file;

        try {
            String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.add("Cookie", "JSESSIONID=" + sessionId);
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());
            requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
            HttpEntity<String> entity = new HttpEntity<>(requestHeaders);

            ResponseEntity<byte[]> response = restTemplate.exchange(UrlConstants.DOWNLOAD_REPORT + reportID, HttpMethod.GET, entity, byte[].class, "1");

            if (response.getStatusCode().equals(HttpStatus.OK)) {
                file = File.createTempFile("report", ".pdf");
                //output = new FileOutputStream(file);
                //FileInputStream fileToDownload = new FileInputStream(file);
                IOUtils.write(response.getBody(), httpServletResponse.getOutputStream());
                httpServletResponse.setContentType("application/pdf");
                httpServletResponse.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getAllReports(String username) {
        String list;
        try {
            list = (String) RestTemplateController.connectRest(UrlConstants.GET_ALL_REPORTS + username, HttpMethod.GET, String.class).getBody();
        } catch (HttpClientErrorException e) {
            throw e;
        }
        return list;
    }

    private String getAllTemplates(String username) {
        String templates;
        try {
            templates = (String) RestTemplateController.connectRest(UrlConstants.GET_ALL_TEMPLATES + username, HttpMethod.GET, String.class).getBody();
        } catch (HttpClientErrorException e) {
            throw e;
        }

        return templates;
    }

    private Template getTemplateByID (int templateID) {
        try {
            return  (Template)RestTemplateController.connectRest(UrlConstants.GET_TEMPLATE_BY_ID + templateID, HttpMethod.GET, Template.class).getBody();
        }
        catch (HttpClientErrorException e) {
            throw  e;
        }
    }
}
