package com.rpgen.mvc.controller;

import com.rpgen.mvc.UrlConstants;
import com.rpgen.mvc.model.Connection;
import com.rpgen.mvc.model.User;
import com.sun.javafx.sg.prism.NGShape;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.ModelAndView;

import javax.enterprise.inject.Model;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by Maria Lisunova on 19.04.2016.
 *
 */

@Controller
@RequestMapping(value = "/database")
@SessionAttributes("userJSP")
public class ConnectionController {
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView returnDataBasePage(@ModelAttribute("userJSP") User user) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            modelAndView.addObject("connections", getAllConnections(user.getLogin()));
            modelAndView.addObject("userJSP", user);
            modelAndView.setViewName("database");
        }
        catch (HttpClientErrorException e) {
            modelAndView.setViewName("redirect:/error");
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping(value="create", method= RequestMethod.GET)
    public ModelAndView returnConnectionForm () {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("addConnection");
        return modelAndView;
    }

    @RequestMapping(value = "create", method = RequestMethod.POST)
    public ModelAndView createConnection (Connection connection, @ModelAttribute("userJSP") User user) {
        ModelAndView modelAndView = new ModelAndView();
        connection.setUserId(user.getUserId());
        try {
            RestTemplateController.connectRest(UrlConstants.CREATE_CONNECTION, HttpMethod.POST, ResponseEntity.class, connection);
            modelAndView.setViewName("redirect:/database");
        }
        catch (HttpClientErrorException e) {
            e.printStackTrace();
            modelAndView.setViewName("redirect:/error");
        }

        return modelAndView;
    }

    private List<Connection> getAllConnections (String username) {
        String uri = UrlConstants.GET_ALL_CONNECTIONS + username;
        List <Connection> list;
        try {
            list = (List<Connection>)RestTemplateController.connectRest(uri, HttpMethod.GET, List.class).getBody();
        }
        catch (HttpClientErrorException e) {
            throw e;
        }

        return list;
    }

    @RequestMapping(value = "/{connectionID}", method = RequestMethod.GET)
    public ModelAndView returnConnectionByID (@PathVariable("connectionID") int connectionID) {
        String uri = UrlConstants.GET_CONNECTION_BY_ID + connectionID;
        ModelAndView modelAndView = new ModelAndView();
        try {
            Connection foundConnection = (Connection)RestTemplateController.connectRest(uri, HttpMethod.GET, Connection.class).getBody();
            modelAndView.addObject("foundConnection", foundConnection);
            modelAndView.setViewName("connectionPage");
            return modelAndView;
        }
        catch (HttpClientErrorException e) {
            modelAndView.setViewName("redirect:/error");
            e.printStackTrace();
            return modelAndView;
        }
    }


    @RequestMapping(value = "/delete/{connectionID}")
    public ModelAndView deleteConnection (@PathVariable("connectionID") int connectionID) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            RestTemplateController.connectRest(UrlConstants.GET_CONNECTION_BY_ID + connectionID, HttpMethod.DELETE, ResponseEntity.class);
            modelAndView.setViewName("redirect:/database");
        }
        catch (HttpClientErrorException e) {
            e.printStackTrace();
            modelAndView.setViewName("redirect:/error");
        }
        return modelAndView;
    }

    @RequestMapping(value="/edit/{connectionID}", method = RequestMethod.GET)
    public ModelAndView returnEditPage (@PathVariable("connectionID") int connectionID) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("editConnection");
        try {
            Connection connection = (Connection) RestTemplateController.connectRest(UrlConstants.GET_CONNECTION_BY_ID + connectionID, HttpMethod.GET, Connection.class).getBody();
            modelAndView.addObject(connection);
            return modelAndView;
        }
        catch (HttpClientErrorException e) {
            modelAndView.setViewName("redirect:/error");
            return modelAndView;
        }
    }

    @RequestMapping(value="/edit/{connectionID}", method = RequestMethod.POST)
    public ModelAndView editConnection (@PathVariable("connectionID") int connectionID, @ModelAttribute("userJSP") User user, Connection updatedConnection) {
        updatedConnection.setConnectionId(connectionID);
        updatedConnection.setUserId(user.getUserId());
        ModelAndView modelAndView = new ModelAndView();
        try {
            RestTemplateController.connectRest(UrlConstants.GET_CONNECTION_BY_ID + connectionID, HttpMethod.PUT, ResponseEntity.class, updatedConnection);
            modelAndView.setViewName("redirect:/database");
            return modelAndView;
        }
        catch (HttpClientErrorException e) {
            modelAndView.setViewName("redirect:/error");
            return modelAndView;
        }
    }

    @RequestMapping(value = "/test", method =  RequestMethod.POST)
    @ResponseBody
    public int testConnection (@ModelAttribute("userJSP") User user, @RequestParam("connectionName") String connectionName, @RequestParam("dBType") String dBType, @RequestParam("dBName") String dBName, @RequestParam("host") String host, @RequestParam("dBUsername") String dBUsername, @RequestParam("dBPassword") String dBPassword, @RequestParam("port") int port, @RequestParam("sid") String sid) {
        Connection connection = new Connection(connectionName, dBType, dBName, dBUsername, dBPassword, host, port, user.getUserId(), sid);

        try {
            return RestTemplateController.connectRest(UrlConstants.TEST_CONNECTION, HttpMethod.POST, Connection.class, connection).getStatusCode().value();

        }
        catch (HttpClientErrorException e) {
            return -1;
        }
    }
}
