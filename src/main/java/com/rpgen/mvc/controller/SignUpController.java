package com.rpgen.mvc.controller;

import com.rpgen.mvc.UrlConstants;
import com.rpgen.mvc.model.User;
import com.sun.javafx.sg.prism.NGShape;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;


/**
 * Created by Maria Lisunova on 19.04.2016.
 *
 */
@Controller
@SessionAttributes("userJSP")
@RequestMapping("/register")
public class SignUpController {
    @Autowired
    @Qualifier("authenticationManager")
    private AuthenticationManager authenticationManager;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView returnRegistrationPage (@RequestParam(value = "error", required = false) String error) {
        ModelAndView modelAndView = new ModelAndView();
        if (error != null) modelAndView.addObject("error", "User with such login already exists!");
        modelAndView.setViewName("signup");
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView saveUser (User user) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            RestTemplateController.postForObject(UrlConstants.REGISTER, user);
            Authentication authenticatedUser = authenticateUser(user);
            modelAndView.setViewName("redirect:/home");
            modelAndView.addObject("userJSP", authenticatedUser);
            return (modelAndView);
        }
        catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.CONFLICT) {
                modelAndView.setViewName("redirect:/register?error=true");
                return modelAndView;
            }
            else {
                modelAndView.setViewName("redirect:/error");
                modelAndView.addObject("error", e.getMessage());
                return (modelAndView);
            }
        }
        catch (AuthenticationException e) {
            modelAndView.setViewName("redirect:/error");
            modelAndView.addObject("error", e.getMessage());
            return (modelAndView);
        }
    }

    private Authentication authenticateUser (User user) {
        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(user.getLogin(), user.getPassword());
            Authentication authenticatedUser = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authenticatedUser);
            return authenticatedUser;
        }
        catch (AuthenticationException e) {
            throw e;
        }
    }
}
