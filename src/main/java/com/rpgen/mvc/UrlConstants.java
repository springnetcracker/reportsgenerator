package com.rpgen.mvc;

/**
 * Created by Maria Lisunova on 02.05.2016.
 *
 */
public class UrlConstants {
    public static final String GET_USER = "http://localhost:8080/ReportsGenerator/core/";
    public static final String CREATE_CONNECTION = "http://localhost:8080/ReportsGenerator/core/connection";
    public static final String GET_ALL_CONNECTIONS = "http://localhost:8080/ReportsGenerator/core/connection/allconn/";
    public static final String GET_CONNECTION_BY_ID = "http://localhost:8080/ReportsGenerator/core/connection/";
    public static final String TEST_CONNECTION = "http://localhost:8080/ReportsGenerator/core/connection/test";
    public static final String CREATE_REPORT = "http://localhost:8080/ReportsGenerator/core/report";
    public static final String GET_TEMPLATE_BY_ID = "http://localhost:8080/ReportsGenerator/core/template/";
    public static final String GET_ALL_REPORTS = "http://localhost:8080/ReportsGenerator/core/report/allreport/";
    public static final String GET_ALL_TEMPLATES = "http://localhost:8080/ReportsGenerator/core/template/alltemplatesuser/";
    public static final String REGISTER = "http://localhost:8080/ReportsGenerator/core/register";
    public static final String CREATE_TEMPLATE = "http://localhost:8080/ReportsGenerator/core/template";
    public static final String GET_REPORT_BY_ID = "http://localhost:8080/ReportsGenerator/core/report/";
    public static final String DOWNLOAD_REPORT = "http://localhost:8080/ReportsGenerator/core/report/pdf/";
    public static final String CHECK_SYNTAXIS = "http://localhost:8080/ReportsGenerator/core/template/check";

}
