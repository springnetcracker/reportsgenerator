package src.main.test.com.rpgen.core.ejb;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import static org.junit.Assert.*;

/**
 * Created by denis on 12.04.16.
 */
public class ReportParamsBeanTest {

    protected static Object reportParams;
    static Method method  = null;
    static Class<?> clazz = null;

    @BeforeClass
    public static void setBeforeClass() throws Exception{
        try{
        clazz=Class.forName("com.rpgen.core.ejb.ReportParamsBean");
        method = clazz.getMethod("identifyParamsFromQuery", new Class[]{String.class});
        reportParams = clazz.newInstance();
        } catch (Exception e){
            e.printStackTrace();
            fail("fail");
        }
    }


    @Test
    public void testIdentifyParamsFromQuery() throws InvocationTargetException {
        List<String> myResult=new ArrayList<String>();
        Object result = null;
        myResult.add("MyParAm1");
        myResult.add("MyParAM4");
        try{
           result=(method.invoke(reportParams, new Object[] {"select * from employees where employee_id=:MyParAm1 and email=:MyParAM4"}));
        } catch (Exception e){
            e.printStackTrace();
            fail("fail in exception");
        }
        assertEquals("Not equals", myResult, result);

    }
}