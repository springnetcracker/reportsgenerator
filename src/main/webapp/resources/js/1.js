"use strict"

function submitLoginForm(param) {
	$(param).submit();
}

function pageDown() {
	$('html,body').stop().animate({ scrollTop: $('#info').offset().top}, 1000);
}

function showUserMenu () {
	$('#user-menu').fadeIn(400);		
}

function hideUserMenu () {
	$('#user-menu').fadeOut(400);		
}

$(document).ready(function() {
	jQuery.validator.setDefaults({
	  debug: true,
	  success: function(label) {
		label.removeClass("error").addClass("success")
	  }
	});
	
	jQuery.validator.addMethod("loginValidation", function(value, element) {
		return this.optional( element ) || /^[a-zA-Z][a-zA-Z0-9_-]+$/.test( value );
	}, 'This field must contain only letters, numbers and symbols "-" and "_".');

	jQuery.validator.addMethod("nameSpaceValidation", function(value, element) {
		return this.optional( element ) || /^[a-zA-Z][a-zA-Z0-9-_\s]+$/.test( value );
	}, 'This field must contain only letters, numbers and namespaces.');

	jQuery.validator.addMethod("passwordValidation", function(value, element) {
		return this.optional( element ) || /^[a-zA-Z0-9_-]+$/.test( value );
	}, 'This field must contain only letters, numbers and symbols "-" and "_".');
	
	jQuery.validator.addMethod("nameValidation", function(value, element) {
		return this.optional( element ) || /^[a-zA-Z_-]+$/.test( value );
	}, 'This field must contain only letters');
	
	jQuery.validator.addMethod("hostValidation", function(value, element) {
		return this.optional( element ) || /^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$/.test( value ) || /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/.test(value);
	}, 'Please enter either an IP address or a url');
	
	
	$('#signup-form').validate({
		submitHandler: function (form) {
			form.submit();
		},
		rules: {
			login: {
				minlength: 4,
				maxlength: 30,
				loginValidation: true
			},
			
			firstName: {
				minlength: 3,
				maxlength: 120,
				nameValidation: true
			},
			
			lastName: {
				minlength: 3,
				maxlength: 30,
				nameValidation: true
			},
			email: {
				minlength: 6,
				maxlength: 50,
				email: true
			}, 
			password: {
				minlength: 6,
				maxlength: 20,
				passwordValidation: true
			},
			
			passwordConfirm: {
				minlength: 6,
				maxlength: 20,
				equalTo: "#password",
				passwordValidation: true
			},

			templateName: {
				minlength: 1,
				maxlength: 50,
				nameSpaceValidation: true
			}
			
		},
		
		messages: {
			login: {
				minlength: "Please enter at least 4 characters",
				maxlenght: "Login must be less than 20 symbols"
			},
			
			firstName: {
				minlength: "First name must be at least 3 symbols",
				maxlenght: "First name must be less than 20 symbols"
			},
			
			lastName: {
				minlength: "Last name must be at least 3 symbols",
				maxlenght: "Last name must be less than 30 symbols"
			},
			email: {
				minlength: "Email must be at least 6 symbols",
				maxlength: "Email must be less than 50 symbols",
				email: "Please enter a valid email"
			}, 
			password: {
				minlength: "Password must be at least 6 symbols",
				maxlength: "Password must be less than 20 symbols"
			},
			
			passwordConfirm: {
				equalTo: "Passwords do not match!"
			},
			templateName: {
				minlength: "Template name must be at least 1 symbols",
				maxlength: "Template name must be less than 50 symbols"
			}
		} 
	});
	$('#login-form').validate({
		rules: {
			login: {
				loginValidation: true
			},
			
			password: {
				loginValidation: true
			}
		}
		
	});

	$('#report-form').validate({
		submitHandler: function (form) {
			form.submit();
		},
		rules: {
			reportName: {
				minlength: 1,
				maxlength: 50,
				nameSpaceValidation: true
			}
		},
		messages: {
				reportName: {
					minlength: "Report name must be at least 1 symbols",
					maxlength: "Report name must be less than 50 symbols"
				}
		}
	});

	$('#add-connection').validate({
		submitHandler: function (form) {
			if ($("#add_conn").attr('disabled') != "true") form.submit();
		},
		rules: {			
			connectionName: {
				minlength: 1,
				maxlength: 50,
				nameSpaceValidation: true
			},
			
			host: {
				hostValidation: true
			},
			dBUsername: {
				minlength: 1,
				maxlength: 20,
				loginValidation: true
			},

			dBPassword: {
				minlength: 1,
				maxlength: 20,
				passwordValidation: true
			},
			
			port: {
				number: true
			}, 
			
			sid: {
				minlength: 2,
				maxlength: 20,
				loginValidation: true
			},
			dBName: {
				minlength: 1,
				maxlength: 40,
				passwordValidation: true
			}
		},

		messages: {
			connectionName: {
				minlength: "Please enter at least 1 characters",
				maxlength: "Please enter no more than 50 characters"
			},

			sid: {
				minlength: "Please enter at least 2 characters",
				maxlength: "Please enter no more than 20 characters"
			},
			dbName: {
				minlength: "Please enter at least 1 characters",
				maxlength: "Please enter no more than 20 characters"
			},
			dBUsername: {
				minlength: "Please enter at least 1 characters",
				maxlength: "Please enter no more than 20 characters"
			},

			dBPassword: {
				minlength: "Please enter at least 1 characters",
				maxlength: "Please enter no more than 20 characters"
			}
		} 
	})
});

function getParamsList(param) {
	var request = $(param).attr('value');
	var target = ":";
	var paramsList = [];
	var pos = -1;
	$('.param').each(function()
	{
		$(this).remove();
	});
	var count = 0;
	while ((pos = request.indexOf(target, pos + 1)) != -1) {
		pos = pos + 1;
		var endParamIndex = request.indexOf(" ", pos);
		if (endParamIndex < pos) endParamIndex = request.length;
		var newParam = request.substring(pos, endParamIndex);
		var select = "<select id='paramType" + count + "' name='paramTypes'> <option> INTEGER </option> <option> TEXT</option> <option> FLOAT </option> </select>";
		$('#paramsList').append("<tr class='param'> <td> <input name='paramVars' id='paramVar" + count + "' value='" + newParam + "' readonly> </input> </td> <td> <input  name='paramNames' type='text' width='100px' height='30px' id='paramName" + count + "' required> </td> <td>" + select + "</td> </tr> ");
		count++;
	}
}

function testConnection() {
	$.ajax({
		url: "database/test",
		type : "POST",
		data: {
			connectionName: $('#connection').val(),
			dBType: $('#dBType').val(),
			dBName: $('#dBName').val(),
			host: $('#hostIP').val(),
			dBUsername: $('#userName').val(),
			dBPassword: $('#password').val(),
			port: $('#port').val(),
			sid: $('#sid').val()
		},
		success: function (data){
			if (data == 200) {
				$('#add_conn').attr("disabled", false);
			}
			else {
				alert ("Test connection was unsuccessful. Please change parameters before you try again.");
			}
		},
		error: function () {
		alert ("Something went wrong:(");
	}
	});
}

function checkSyntaxis() {
	$.ajax({
		url: "template/check",
		type : "POST",
		data: {
			connectionName: $('#db').val(),
			templateName: $('#template').val(),
			templateQuery: $('#sqlRequest').val(),
			paramNames: getParamNames(),
			paramVars: getParamVars(),
			paramTypes: getParamTypes()
		},
		success: function (data){
			if (data == 200) {
				$('#add_template_button').attr("disabled", false);
			}
			else {
				alert ("Syntaxis is incorrect. Please change sql request before you try again.");
			}
		},
		error: function () {
			alert ("Something went wrong:(");
		}
	});


}

function getParamNames () {
	var arrayOfNames = [];
	var count = $('.param').length;
	for (var i = 0; i < count; i++) {
		var curr = $("#paramName"+i).val();
		arrayOfNames.push(curr);
		}
	return arrayOfNames;
	}

function getParamVars () {
	var arrayOfVars = [];
	var count = $('.param').length;
	for (var i = 0; i < count; i++) {
		var curr = $("#paramVar"+i).val();
		arrayOfVars.push(curr);
	}
	return arrayOfVars;
}

function getParamTypes () {
	var arrayOfTypes = [];
	var count = $('.param').length;
	for (var i = 0; i < count; i++) {
		var curr = $("#paramType"+i).val();
		arrayOfTypes.push(curr);
	}
	return arrayOfTypes;
}