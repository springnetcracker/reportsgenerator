<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <title>Reports Generator</title>
        <link rel="stylesheet" href="css/style.css">
        <link href='https://fonts.googleapis.com/css?family=Forum&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>
        <script src="js/jquery-min.js"> </script>
        <script src="js/1.js"> </script>
        <script src="js/jquery-validate.min.js"> </script>
    </head>
<body>
<div class="userpage">
    <div class="head">
        <a href="/ReportsGenerator" id="log-pic"> <img src="img/logo.png" width="30px" height="30px"> </img> </a>
        <h3> Reports Generator </h3>
			<span id="avatar">
				<a href="" class="avatar-pic"> <img src="img/avatar.png" width="30px" height="30px"> </img>  </a>
				<a href="" class="user-name"> Login </a>
			</span>
        <div class="clearfix"></div>
    </div>
    <form class="signup-form" id="login-form-id" method="POST" action="j_spring_security_check">
        <ul>
            <li>
                <h2>Login</h2>
            </li>
            <li>
                <c:if test="${not empty error}">
                    <img id="error-pic" src="css/img/invalid.png"> </img> <h3 id="error-message"> ${error} </h3>
                </c:if>
            </li>

            <li>
                <label for="userName" id="parameters">Login:</label>
                <input type="text" id="userName" name="login"  placeholder="Login" required />
            </li>
            <li>
                <label for="password" id="parameters">Password:</label>
                <input id="password" type="password" name="password"   placeholder="Password" required />
            </li>
            <li>
                <button name="submit" type="submit" class="submit"> Log in </button>
            </li>
        </ul>
    </form>
</div>
</body>
</html>
