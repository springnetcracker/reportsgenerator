<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <title>Reports Generator</title>
    <base href="http://162.243.2.65:8080/ReportsGenerator/" />
    <link rel="stylesheet" href="resources/css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Forum&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>
    <script src="resources/js/jquery-min.js"> </script>
    <script src="resources/js/1.js"> </script>
    <script src="resources/js/jquery-validate.min.js"> </script>
</head>
<body>
<div class="userpage">
    <div class="head">
        <a href="home" id="log-pic"> <img src="resources/img/logo.png" width="30px" height="30px"> </img> </a>
        <h3> Reports Generator </h3>
			<span id="avatar">
				<a href="#user-menu" onmouseenter="showUserMenu();" class="avatar-pic"> <img src="img/avatar.png" width="30px" height="30px"> </img>  </a>
				<a href="#user-menu" onmouseenter="showUserMenu();" class="user-name"> ${userJSP.firstName} ${userJSP.lastName} </a>
			</span>
        <div class="clearfix"></div>
    </div>
    <ul class="menu">
        <li> <a href="report"> <img src="img/PDF.png" width="60px" height="60px" id="sett-pic"> <h3> Reports </h3> </img> </a> </li>
        <li> <a href="database"> <img src="resources/img/database-512.png" width="60px" height="60px" id="db-pic"> <h3> Data Bases </h3> </img> </a> </li>
        <li> <a href="template"> <img src="img/template.png" width="60px" height="60px" id="temp-pic"> <h3> Templates </h3> </img> </a> </li>
    </ul>
    <form class="signup-form" id="signup-form" method="post" action="template/create">
        <ul>
            <li>
                <h2>Adding a new template</h2>
            </li>
            <li>
                <label for="template" id="parameters">Template name:</label>
                <input type="text" id="template" name="templateName" placeholder="Template name" required />
            </li>

            <li>
                <c:if test="${connections == null}">
                    <img id="error-pic" src="css/img/invalid.png"> </img> <h3 id="error-message"> Please add connections before you can create a template </h3>
                </c:if>
            </li>

            <li>
                <label for="db" id="parameters">Choose a database:</label>
                <select id="db" required name="connectionName">
                    <c:if test="${connections != null}">
                        <c:forEach items="${connections}" var="connection">
                            <option id=${connection.connectionId}> ${connection.connectionName}  </option>
                        </c:forEach>
                    </c:if>
                    <c:if test="${connections == null}">
                        <option> ... </option>
                    </c:if>
                </select>
            </li>
            <li>
                <Label for="sqlRequest" id="parameters" > Type your sql request here: </Label>
                <textarea onkeyup="getParamsList('#sqlRequest');" id="sqlRequest" name="templateQuery" placeholder="Type your sql request here" cols="40" rows="10"> </textarea>
            </li>
            <li>
                <table id="paramsList">
                    <th>Parameter</th><th>Name</th><th>Type</th>

                </table>
            </li>
            <li>
                <button type="submit" class="submit" id="add_template_button" disabled="disabled"> Add </button>
                <button class="submit" id="check" type="button" onclick="checkSyntaxis();">Check syntaxis</button>
            </li>
        </ul>
    </form>
    <ul id="user-menu" onmouseleave="hideUserMenu();">
        <li> <a href="j_spring_security_logout"> Log out </a> </li>
    </ul>
</div>
</body>
</html>
