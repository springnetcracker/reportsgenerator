<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <meta charset="utf-8">
    <title>Reports Generator</title>
    <base href="http://162.243.2.65:8080/ReportsGenerator/" />
    <link rel="stylesheet" href="css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Forum&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>
    <script src="js/jquery-min.js"> </script>
    <script src="js/1.js"> </script>
    <script src="js/jquery-validate.min.js"> </script>
</head>
<body>
<div class="userpage">
    <div class="head">
        <a href="home" id="log-pic"> <img src="img/logo.png" width="30px" height="30px"> </img> </a>
        <h3> Reports Generator </h3>
			<span id="avatar">
				<a href="#user-menu" onmouseenter="showUserMenu();" class="avatar-pic"> <img src="img/avatar.png" width="30px" height="30px"> </img>  </a>
				<a href="#user-menu" onmouseenter="showUserMenu();" class="user-name"> ${userJSP.firstName} ${userJSP.lastName} </a>
			</span>
        <div class="clearfix"></div>
    </div>
    <ul class="menu">
        <li> <a href="report"> <img src="img/PDF.png" width="60px" height="60px" id="sett-pic"> <h3> Reports </h3> </img> </a> </li>
        <li> <a href="database"> <img src="img/database-512.png" width="60px" height="60px" id="db-pic"> <h3> Data Bases </h3> </img> </a> </li>
        <li> <a href="template"> <img src="img/template.png" width="60px" height="60px" id="temp-pic"> <h3> Templates </h3> </img> </a> </li>
    </ul>
    <form class="signup-form" id="edit-connection" method="POST" action="database/edit/${connection.connectionId}">
        <ul>
            <li>
                <h2>Editing a DB connection</h2>
            </li>
            <li>
                <label for="connection" id="parameters">Connection name:</label>
                <input type="text" id="connection" name="connectionName" value="${connection.connectionName}" required />
            </li>
            <li>
                <label for="dBType" id="parameters">DB type:</label>
                <select name="dBType" id="dBType" required>
                    <option id="oracle"> Oracle </option>
                    <option id="mySQL"> MySQL </option>
                    <option id="msSQL"> MS SQL </option>
                </select>
            </li>
            <li>
                <label for="dBName" id="parameters">DB name:</label>
                <input type="text" id="dBName" name="dBName" value="${connection.dBName}" required />
            </li>
            <li>
                <label for="hostIP" id="parameters">Host IP:</label>
                <input type="text" id="hostIP" name="host"  value="${connection.host}" required />
            </li>
            <li>
                <label for="userName" id="parameters">Username:</label>
                <input type="text" id="userName" name="dBUsername"  value="${connection.dBUsername}" required />
            </li>

            <li>
                <label for="password" id="parameters">Password:</label>
                <input id="password" type="password" name="dBPassword"   value="${connection.dBPassword}" required />
            </li>
            <li>
                <label for="port" id="parameters">Port:</label>
                <input type="text" id="port" name="port"   value="${connection.port}" required />
            </li>
            <li>
                <label for="sid" id="parameters">SID:</label>
                <input type="text" id="sid" name="sid" value="${connection.sid}" required />
            </li>

            <li>
                <button class="submit" id="add_conn" type="submit" disabled="disabled">Save</button>
                <button class="submit" id="test" type="button" onclick="testConnection();">Test connection</button>
            </li>
        </ul>
    </form>
    <ul id="user-menu" onmouseleave="hideUserMenu();">
        <li> <a href="j_spring_security_logout"> Log out </a> </li>
    </ul>
</div>
</body>
</html>
