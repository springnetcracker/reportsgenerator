<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <meta charset="utf-8">
    <title>Reports Generator</title>
    <base href="http://162.243.2.65:8080/ReportsGenerator/" />
    <link rel="stylesheet" href="resources/css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Forum&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>
    <script src="resources/js/jquery-min.js"> </script>
    <script src="resources/js/1.js"> </script>
    <script src="resources/js/jquery-validate.min.js"> </script>
</head>
<body>
<div class="userpage">
    <div class="head">
        <a href="home" id="log-pic"> <img src="resources/img/logo.png" width="30px" height="30px"> </img> </a>
        <h3> Reports Generator </h3>
			<span id="avatar">
				<a href="#user-menu" onmouseenter="showUserMenu();" class="avatar-pic"> <img src="img/avatar.png" width="30px" height="30px"> </img>  </a>
				<a href="#user-menu" onmouseenter="showUserMenu();" class="user-name"> ${userJSP.firstName} ${userJSP.lastName} </a>
			</span>
        <div class="clearfix"></div>
    </div>
    <ul class="menu">
        <li> <a href="report"> <img src="img/PDF.png" width="60px" height="60px" id="sett-pic"> <h3> Reports </h3> </img> </a> </li>
        <li> <a href="database"> <img src="resources/img/database-512.png" width="60px" height="60px" id="db-pic"> <h3> Data Bases </h3> </img> </a> </li>
        <li> <a href="template"> <img src="img/template.png" width="60px" height="60px" id="temp-pic"> <h3> Templates </h3> </img> </a> </li>
    </ul>
    <form class="signup-form" id="signup-form" action="report/create/next" method="post">
        <ul>
            <li>
                <h2>Creating a report</h2>
            </li>
            <li>
                <label for="report" id="parameters">Report name: </label>
                <h3 id="report"> ${report.reportName} </h3>
            </li>
            <li>
                <table id="reportParamsList"  >
                    <th>Parameter</th><th>Name</th><th>Type</th><th>Value</th>
                    <c:forEach items="${reportParameters}" var="reportParam" >
                        <tr>
                            <td> ${reportParam.paramName}</td>
                            <td> ${reportParam.varParam}</td>
                            <td> ${reportParam.typeVarParam}</td>
                            <td> <input type="text" name="values" id="paramValues" placeholder="Parameter value" required> </td>
                        </tr>
                    </c:forEach>
                </table>
            </li>
            <li>
                <button type="submit" class="submit"> Create </button>
            </li>
        </ul>
    </form>
    <ul id="user-menu" onmouseleave="hideUserMenu();">
        <li> <a href="j_spring_security_logout"> Log out </a> </li>
    </ul>
</div>
</body>
</html>
