<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <meta charset="utf-8">
    <title>Reports Generator</title>
    <base href="http://162.243.2.65:8080/ReportsGenerator/" />
    <link rel="stylesheet" href="css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Forum&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>
    <script src="js/jquery-min.js"> </script>
    <script src="js/1.js"> </script>
    <script src="js/jquery-validate.min.js"> </script>
</head>
<body>
<div class="userpage">
    <div class="head">
        <a href="/ReportsGenerator" id="log-pic"> <img src="img/logo.png" width="30px" height="30px"> </img> </a>
        <h3> Reports Generator </h3>
			<span id="avatar">
				<a href="login" class="avatar-pic"> <img src="img/avatar.png" width="30px" height="30px"> </img>  </a>
				<a href="login" class="user-name"> Login </a>
			</span>
        <div class="clearfix"></div>
    </div>
    <form class="signup-form" id="signup-form" method="post" action="register">
        <ul>
            <li>
                <h2>Registration</h2>
            </li>
            <li>
                <c:if test="${not empty error}">
                    <img id="error-pic" src="css/img/invalid.png"> </img> <h3 id="error-message"> ${error} </h3>
                </c:if>
            </li>
            <li>
                <label for="name" id="parameters">Name:</label>
                <input type="text" id="name" name="firstName" placeholder="Your name" required />
            </li>
            <li>
                <label for="lastName" id="parameters">Last Name:</label>
                <input type="text" id="lastName" name="lastName"  placeholder="Your last name" required />
            </li>
            <li>
                <label for="userName" id="parameters">Login:</label>
                <input type="text" id="userName" name="login"  placeholder="Login" required />
            </li>
            <li>
                <label for="password" id="parameters">Password:</label>
                <input id="password" type="password" name="password"   placeholder="Password" required />
            </li>
            <li>
                <label for="password" id="parameters">Confirm password:</label>
                <input type="password" name="passwordConfirm"   placeholder="Confirm password" required />
            </li>
            <li>
                <label for="email" id="parameters">Email:</label>
                <input type="email" id="email" name="email" placeholder="Your email" required />
                <span class="form_hint">Proper format "name@something.com"</span>
            </li>

            <li>
                <button class="submit" type="submit" id="registration">Sign Up</button>
            </li>
        </ul>
    </form>
</div>
</body>
</html>
