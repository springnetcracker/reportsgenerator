<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
  <meta charset="utf-8">
  <meta name="keywords" content="reports, DB, database, report generator online, reports generator">
  <title>Reports Generator</title>
  <link rel="stylesheet" href="css/style.css">
  <link href='https://fonts.googleapis.com/css?family=Forum&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Khula' rel='stylesheet' type='text/css'>
  <script src="js/jquery-min.js"> </script>
  <script src="js/1.js"> </script>
  <script src="js/jquery-validate.min.js"> </script>
</head>
<body>
<div class="mainpage">
  <div class="header">
    <h1> Reports Generator </h1>
    <h3> Create reports easily </h3>
    <img src="img/database-512.png" id="db"> </img>
    <a href="register" id="register"> Register </a>
  </div>
  <div class="head">
    <a href="" id="log-pic"> <img src="img/logo.png" width="30px" height="30px"> </img> </a>
    <h3> Reports Generator </h3>
			<span id="avatar">
				<a href="home" class="avatar-pic"> <img src="img/avatar.png" width="30px" height="30px"> </img>  </a>
				<a href="home" class="user-name"> Login </a>
			</span>
    <div class="clearfix"></div>
  </div>
  <img src="img/arrow.png" id="arrow" onclick="pageDown();"> </img>
  <div id="info">
    <h3> How to create the necessary reports easily? </h3>
    <div class="cont">
      <img src="img/database-512.png" class="block" width="50px" height="50px">
      <h3> Add DataBases </h3>
    </div>
    <div class="cont">
      <img src="img/logo.png" class="block" width="50px" height="50px">
      <h3> Create a report </h3>
    </div>
    <div class="cont">
      <img src="img/arrow.png" class="block" width="50px" height="50px">
      <h3> Download </h3>
    </div>
  </div>
  <div class="footer">
  </div>
</div>
</body>
</html>
