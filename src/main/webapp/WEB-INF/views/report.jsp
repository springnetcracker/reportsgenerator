<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="utf-8">
    <meta name="keywords" content="reports, DB, database, report generator online">
    <title>Reports Generator</title>
    <link rel="stylesheet" href="css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Forum&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>
    <script src="js/jquery-min.js"> </script>
    <script src="js/1.js"> </script>
    <script src="js/jquery-validate.min.js"> </script>
</head>
<body>
<div class="userpage">
    <div class="head">
        <a href="home" id="log-pic"> <img src="img/logo.png" width="30px" height="30px"> </img> </a>
        <h3> Reports Generator </h3>
			<span id="avatar">
				<a href="#user-menu" onmouseenter="showUserMenu();" class="avatar-pic"> <img src="img/avatar.png" width="30px" height="30px"> </img>  </a>
				<a href="#user-menu" onmouseenter="showUserMenu();" class="user-name"> ${userJSP.firstName} ${userJSP.lastName} </a>
			</span>
        <div class="clearfix"></div>
    </div>
    <ul class="menu">
        <li> <a href="report"> <img src="img/PDF.png" width="60px" height="60px" id="sett-pic"> <h3> Reports </h3> </img> </a> </li>
        <li> <a href="database"> <img src="img/database-512.png" width="60px" height="60px" id="db-pic"> <h3> Data Bases </h3> </img> </a> </li>
        <li> <a href="template"> <img src="img/template.png" width="60px" height="60px" id="temp-pic"> <h3> Templates </h3> </img> </a> </li>
    </ul>
    <div class="db-content">
        <form>
            <a href="report/create" id="createReport"> <h3> Create a new report</h3> </a>
            <table id="reportsTable">
                <th>Report Name </th><th>Date</th><th>Status</th> <th> Download</th>
                <c:forEach items="${reports}" var="report">
                    <tr>
                        <td> <a href="report/${report.reportId}"> <h3>  ${report.reportName} </h3> </a> </td>
                        <td> <h3> ${report.reportDate}</h3></td>
                        <td> <h3> <c:if test="${report.status == 0}"> In progress </c:if> <c:if test="${report.status == 1}"> Done </c:if> </h3> </td>
                        <td> <h3> <c:if test="${report.status == 0}"> Report is being created </c:if> <c:if test="${report.status == 1}"> <a href="report/download/${report.reportId}"> Download  </a> </h3> </c:if>
                        </td>
                        <td>
                            <a href="report/delete/${report.reportId}" > <img src="img/delete.png" width="20px" height="20px"> </img> </a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </form>
    </div>
    <div id="overlay" onclick="closeForm();"> </div>
    <ul id="user-menu" onmouseleave="hideUserMenu();">
        <li> <a href="j_spring_security_logout"> Log out </a> </li>
    </ul>
</div>
</body>
</html>
