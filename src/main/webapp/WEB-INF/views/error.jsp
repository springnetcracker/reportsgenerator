<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error</title>
    <meta charset="utf-8">
    <meta name="keywords" content="reports, DB, database, report generator online">
    <title>Reports Generator</title>
    <link rel="stylesheet" href="css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Forum&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>
    <script src="js/jquery-min.js"> </script>
    <script src="js/1.js"> </script>
    <script src="js/jquery-validate.min.js"> </script>
</head>
<body>
    <div class="error_page">
        <h1> Something went wrong </h1>
        <img src="img/panda.jpeg">
        <a href="/ReportsGenerator/home"> <h3 class="error_button"> Home page </h3> </a>
    </div>
</body>
</html>
