<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <meta charset="utf-8">
    <title>Reports Generator</title>
    <base href="http://162.243.2.65:8080/ReportsGenerator/" />
    <link rel="stylesheet" href="css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Forum&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>
    <script src="js/jquery-min.js"> </script>
    <script src="js/1.js"> </script>
    <script src="js/jquery-validate.min.js"> </script>
</head>
<body>
<div class="userpage">
    <div class="head">
        <a href="home" id="log-pic"> <img src="img/logo.png" width="30px" height="30px"> </img> </a>
        <h3> Reports Generator </h3>
			<span id="avatar">
				<a href="#user-menu" onmouseenter="showUserMenu();" class="avatar-pic"> <img src="img/avatar.png" width="30px" height="30px"> </img>  </a>
				<a href="#user-menu" onmouseenter="showUserMenu();" class="user-name"> ${userJSP.firstName} ${userJSP.lastName} </a>
			</span>
        <div class="clearfix"></div>
    </div>
    <ul class="menu">
        <li> <a href="report"> <img src="img/PDF.png" width="60px" height="60px" id="sett-pic"> <h3> Reports </h3> </img> </a> </li>
        <li> <a href="database"> <img src="img/database-512.png" width="60px" height="60px" id="db-pic"> <h3> Data Bases </h3> </img> </a> </li>
        <li> <a href="template"> <img src="img/template.png" width="60px" height="60px" id="temp-pic"> <h3> Templates </h3> </img> </a> </li>
    </ul>
    <form class="signup-form">
        <ul>
            <li>
                <h2>${foundTemplate.templateName}</h2>
            </li>
            <li>
                <label for="dBName" id="parameters">Connection:</label>
                <a href="database/${foundTemplate.connctionID}"> <h3 id="dBName"> ${connectionName}</h3> </a>
            </li>
            <li>
                <label for="date" id="parameters">Date of creation:</label>
                <h3 id="date"> ${foundTemplate.templateDate}</h3>
            </li>
            <li>
                <label for="templateQuery" id="parameters">SQL request:</label>
                <h3 id="templateQuery"> ${foundTemplate.templateQuery}</h3>
            </li>
            <li>
                <label for="paramsList" id="parameters">Parameters:</label>
                <table id="paramsList">
                    <th>Parameter</th><th>Name</th><th>Type</th>
                    <c:forEach items="${foundTemplate.reportParams}" var="reportParam">
                        <tr>
                            <td> ${reportParam.varParam}</td>
                            <td> ${reportParam.paramName}</td>
                            <td> ${reportParam.typeVarParam}</td>
                        </tr>
                    </c:forEach>
                </table>
            </li>

        </ul>
    </form>
    <ul id="user-menu" onmouseleave="hideUserMenu();">
        <li> <a href="j_spring_security_logout"> Log out </a> </li>
    </ul>
</div>
</body>
</html>

